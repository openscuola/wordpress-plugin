# linux@studenti Wordpress plugins

This folder collects all the plugins that are used within the linux@studenti site. It is synced with the main linux@studenti environment therefore committing here will effectively changed production behaviour, therefore it is advised to test in your local environment before committing.

## Introduction

Testing and development is all done by means of docker containers. This is necessary because we are testing a Wordpress plugin, which means that we need both a webserver with PHP and MySQL, not using docker would otherwise require either setting up these services everytime a development environment is used, or using a virtual machine, which obviously causes a lot of additional overhead whenever we are setting a new development environment.

## Prerequsites

To start developing the wordpress plugins the first step is obviously to clone the gitlab repository:

```bash
git clone https://gitlab.com/openscuola/wordpress-plugin.git
```

Note that if you want to be able to push your commits to the repository you might also want to set up an SSH key [[https://docs.gitlab.com/ee/user/ssh.html]].


## Running docker

To run the containers you need to have docker and docker-compose installed. They are generally available in a standard docker installation, if you're not familiar with docker make sure to get a quick overview by skimming through the official documentation [[https://docs.docker.com/get-started/]]. 

In order to run your container you can simply use the following command in the main repository folder (ie. where the docker-compose.yml file is located):
```bash
docker-compose up
```

This will download the wordpress and MySQL images from the docker repository and start the containers immediately. After this procedure your testing environment will be available at [[http://localhost:8080]], and you will be ready to start developing!

## Editing a plugin

By means of bind mounts you can edit in real time the files in your git repository and see the changes instantly on your deployed docker container. This means that you can edit on your favorite IDE or text editor any file in the plugin folders in the wordpress directory and have the changes reflected immediately on your container at [[http://localhost:8080]].

## Configuring the plugins

In order to not share sensitive information on the gitlab repository, the config files need to be manually configured. In most cases it will be sufficient to just copy the config.ini.sample files and rename them to config.ini, a notable exception is the "gestione-corsi" plugin, which require a valid SMTP server and email address to be configured.

