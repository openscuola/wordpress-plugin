-- Set up the necessary permissions for the newly created databases

CREATE USER 'wpuser'@'%' IDENTIFIED BY 'wppass'; 
GRANT ALL PRIVILEGES ON wpdb.* TO 'wpuser'@'%';

CREATE USER 'openscuola'@'%' IDENTIFIED BY 'openscuola'; 
GRANT ALL PRIVILEGES ON openscuola.* TO 'openscuola'@'%';

CREATE USER 'iscrizioni_user'@'%' IDENTIFIED BY 'iscrizioni_pass'; 
GRANT ALL PRIVILEGES ON iscrizioni.* TO 'iscrizioni_user'@'%';