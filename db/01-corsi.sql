-- Adminer 4.8.1 MySQL 5.7.39 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `iscrizioni` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `iscrizioni`;


DROP TABLE IF EXISTS `corso`;
CREATE TABLE `corso` (
  `nome_corso` varchar(64) NOT NULL,
  `id_corso` varchar(32) NOT NULL,
  `inizio_iscrizioni` date NOT NULL,
  `fine_iscrizioni` date NOT NULL,
  `presenze_obbligatorie` int(8) NOT NULL DEFAULT '7',
  PRIMARY KEY (`id_corso`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `corso` (`nome_corso`, `id_corso`, `inizio_iscrizioni`, `fine_iscrizioni`, `presenze_obbligatorie`) VALUES
('Corso GNU/Linux Base Autunno 2021',	'linux_base_autunno_21',	'2021-11-05',	'2022-01-20',	8),
('Corso GNU/Linux Base Autunno 2022',	'linux_base_autunno_22',	'2022-10-18',	'2023-01-15',	3);

DROP TABLE IF EXISTS `iscritto`;
CREATE TABLE `iscritto` (
  `nome` varchar(64) NOT NULL,
  `cognome` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `matricola` varchar(8) NOT NULL,
  `telefono` varchar(32) NOT NULL,
  `id_corso` varchar(32) NOT NULL,
  `token` char(32) NOT NULL,
  `confermato` int(1) unsigned zerofill NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `attestato_inviato` int(1) unsigned zerofill NOT NULL,
  PRIMARY KEY (`id_corso`,`email`),
  UNIQUE KEY `token` (`token`),
  CONSTRAINT `iscritto_ibfk_2` FOREIGN KEY (`id_corso`) REFERENCES `corso` (`id_corso`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `iscritto` (`nome`, `cognome`, `email`, `matricola`, `telefono`, `id_corso`, `token`, `confermato`, `timestamp`, `attestato_inviato`) VALUES
('Francesco',	'Brozzu',	'francescobrozzu@gmail.com',	'234545',	'0750011669',	'linux_base_autunno_22',	'5fc07375eedffc254bac8bb2d7ba0fb9',	1,	'2023-01-08 16:01:50',	1),
('John',	'Doe',	'francescobrozzu@live.it',	'99999',	'3232312312',	'linux_base_autunno_22',	'4aad287a032534f89ae9ee642b796793',	1,	'2023-01-08 18:01:38',	0);

DROP TABLE IF EXISTS `lezione`;
CREATE TABLE `lezione` (
  `password` varchar(32) NOT NULL,
  `id_corso` varchar(32) NOT NULL,
  `numero_lezione` int(11) NOT NULL,
  `lezione_obbligatoria` tinyint(1) unsigned zerofill NOT NULL,
  PRIMARY KEY (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lezione` (`password`, `id_corso`, `numero_lezione`, `lezione_obbligatoria`) VALUES
('aristotele',	'linux_base_autunno_22',	1,	0),
('cleopatra',	'linux_base_autunno_22',	6,	0),
('copernico',	'linux_base_autunno_22',	7,	0),
('diocleziano',	'linux_base_autunno_22',	2,	1),
('eiadas',	'linux_base_autunno_22',	9,	1),
('machiavelli',	'linux_base_autunno_22',	3,	0),
('montalcini',	'linux_base_autunno_22',	4,	0),
('montessori',	'linux_base_autunno_22',	8,	0),
('napoleone',	'linux_base_autunno_22',	5,	0);

DROP TABLE IF EXISTS `presenza`;
CREATE TABLE `presenza` (
  `token` char(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `id_corso` varchar(32) NOT NULL,
  PRIMARY KEY (`password`,`token`),
  CONSTRAINT `presenza_ibfk_1` FOREIGN KEY (`password`) REFERENCES `lezione` (`password`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `presenza` (`token`, `password`, `id_corso`) VALUES
('4aad287a032534f89ae9ee642b796793',	'aristotele',	'linux_base_autunno_22'),
('5fc07375eedffc254bac8bb2d7ba0fb9',	'aristotele',	'linux_base_autunno_22'),
('5fc07375eedffc254bac8bb2d7ba0fb9',	'cleopatra',	'linux_base_autunno_22'),
('5fc07375eedffc254bac8bb2d7ba0fb9',	'copernico',	'linux_base_autunno_22'),
('4aad287a032534f89ae9ee642b796793',	'diocleziano',	'linux_base_autunno_22'),
('4aad287a032534f89ae9ee642b796793',	'eiadas',	'linux_base_autunno_22'),
('5fc07375eedffc254bac8bb2d7ba0fb9',	'eiadas',	'linux_base_autunno_22'),
('4aad287a032534f89ae9ee642b796793',	'montalcini',	'linux_base_autunno_22'),
('5fc07375eedffc254bac8bb2d7ba0fb9',	'montessori',	'linux_base_autunno_22'),
('4aad287a032534f89ae9ee642b796793',	'napoleone',	'linux_base_autunno_22'),
('5fc07375eedffc254bac8bb2d7ba0fb9',	'napoleone',	'linux_base_autunno_22');

-- 2023-01-09 17:25:31
