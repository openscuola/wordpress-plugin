#!/usr/bin/env python3

import subprocess
import os
import glob
import smtplib
import ssl
import config # config file for SMTP
import logging

import mysql.connector

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import email.utils

from datetime import date, datetime
from babel.dates import format_date

def strip_non_ascii(string):
    ''' Returns the string without non ASCII characters'''
    stripped = (c for c in string if 0 < ord(c) < 127)
    return ''.join(stripped)

def encoding_fix(that: str) -> str:
	replacements = {
		"Ã¨": "è",
		"Ã¬": "ì",
		"Ã\xa9": "é",
		"Ã²": "ò",
		"Ã¹": "ù",
		"Ã": "à",
		"â€™": "'",
	}
	for replacement in replacements:
		that = that.replace(replacement, replacements[replacement])
	return that

def filenamer(curr_user_data):
	fn = gigaclean("attestato_" + strip_non_ascii(curr_user_data['nome']) + "_" + strip_non_ascii(curr_user_data['cognome']) + "_" + curr_user_data['token'] + ".tex")
	return fn

def gigaclean(that: str) -> str:
	return that.replace('*', '_').replace('?', '_').replace(' ', '_').replace('\'', '_').replace('/', '').replace('\\', '').lower()

def verifyAttendance(attendance_data, required_attendance, required_lectures):
	attendance_data = {x.lower() for x in attendance_data}
	required_lectures = {x.lower() for x in required_lectures}
	if (len(attendance_data) < required_attendance):
		print("Not enough classes")
		return False
	if required_lectures.issubset(attendance_data):
		return True
	else:
		logging.error("Required lectures is: " + str(required_lectures))
		logging.error("Attendance data is: " + str(attendance_data))
		logging.error("No required lecture")
		return False

def sendEmail(template, curr_user_data, dry_run):
	fn = filenamer(curr_user_data)
	logging.info("Generating " + fn)
	templated = template
	print(curr_user_data)

	headers = ["nome", "cognome", "nome_corso"]
	for var in headers:
		templated = templated.replace("[" + var.upper() + "]", curr_user_data[var])

	templated = templated.replace("[DATA]", format_date(date.today(), format='long', locale='it'))

	texfile = "temp/" + fn
	outfile = "output/" + fn.replace(".tex", ".pdf")
	with open(texfile, "w", encoding='utf8') as f:
		f.write(templated)
	# Do it twice due to tikz
	subprocess.call([config.PDFLATEX, "-interaction=batchmode", texfile], env={"LD_LIBRARY_PATH":"/home/openscuola/opt/glibc-2.14/lib"})
	subprocess.call([config.PDFLATEX, "-interaction=batchmode", texfile], env={"LD_LIBRARY_PATH":"/home/openscuola/opt/glibc-2.14/lib"})

	os.rename(fn.replace(".tex", ".pdf"), outfile)
	for filename in glob.glob(fn.replace(".tex", ".*")):
		os.remove(filename)
	
	# Send email

	msg = MIMEMultipart()
	msg['Subject'] = 'Invio attestato partecipazione corso ' + curr_user_data['nome_corso']
	msg['From'] = config.sender
	msg['To'] = curr_user_data['email']
	msg['Date'] = email.utils.format_datetime(datetime.now())


	# attach email body
	email_body = 'Gentile ' + curr_user_data['nome'] + ' ' + curr_user_data['cognome'] + \
	',\nIn allegato troverà  l\'attestato di partecipazione al ' + curr_user_data['nome_corso'] + \
	'.\nCordali Saluti \nIl Team di Linux@studenti'
	msg.attach(MIMEText(email_body, 'plain'))

	# attach certificate
	attachment = open(outfile, "rb")
	p = MIMEBase('application', 'octet-stream')
	# To change the payload into encoded form
	p.set_payload((attachment).read())

	encoders.encode_base64(p)
	p.add_header('Content-Disposition', "attachment; filename= %s" % fn.replace(".tex", ".pdf"))

	msg.attach(p)

	context = ssl.create_default_context()
	email_sent = 1
	if (dry_run):
		return email_sent
	else:
		with smtplib.SMTP_SSL(config.server, config.port, context=context) as server:
			try:
				server.login(config.username, config.password)
				server.sendmail(config.sender, curr_user_data['email'], msg.as_string())
			except:
				email_sent = 0
		return email_sent

def main():
	logging.basicConfig(filename="generazione_attestati.log", level=logging.DEBUG)
	template = ""
	with open("template.tex", "r", encoding='utf8') as f:
		template = f.read()

	courses = {}

	cnx = mysql.connector.connect(user=config.username_mysql, password=config.password_mysql,
                              host=config.host_mysql,
                              database=config.database, charset='latin1')
	cursor = cnx.cursor()

	# Obtain information about courses
	query_courses = ("SELECT id_corso, nome_corso, presenze_obbligatorie FROM corso")
	cursor.execute(query_courses)

	for row in cursor:
		courses.update({row[0]: {
			"nome_corso": row[1],
			"presenze_obbligatorie": row[2],
			"lezioni_obbligatorie": set()
		}})

	for curr_course in courses.keys():
		query_lectures = ("SELECT password, numero_lezione, lezione_obbligatoria \
		FROM lezione WHERE id_corso = %s")
		cursor.execute(query_lectures, (curr_course, ))

		for row in cursor:
			if row[2] > 0:
				courses.get(curr_course).get("lezioni_obbligatorie").add(row[0])
	
	
	# Loop through courses
	for course in courses.keys():
		query = ("SELECT iscritto.nome, iscritto.cognome, iscritto.id_corso, iscritto.email, \
		iscritto.attestato_inviato, iscritto.token, presenza.password \
        FROM presenza LEFT JOIN iscritto ON iscritto.token = presenza.token \
	    WHERE iscritto.id_corso = %s AND iscritto.attestato_inviato != 1")

		course_dict = courses.get(course)
		required_lectures = course_dict['lezioni_obbligatorie']
		required_attendance = course_dict['presenze_obbligatorie']

		sent_emails = 0

		cursor.execute(query, (course,))

		certificates_to_be_sent = {}
		attendances_data = {}
		# consume the buffer and store attendance data for user
		for row in cursor:
			if row[5] not in attendances_data.keys():
				attendances_data.update({
					row[5]: set()
				})
				attendances_data[row[5]].add(row[6])
				certificates_to_be_sent.update({
					row[5]: {
						'nome': encoding_fix(row[0]),
						'cognome': encoding_fix(row[1]),
						'nome_corso': course_dict['nome_corso'],
						'email': row[3],
						'token': row[5]
					}
				})
			else:
				attendances_data[row[5]].add(row[6])

		# For each user verify required lectures and send the certificate
		for curr_user_token, curr_user_data in zip(certificates_to_be_sent.keys(), certificates_to_be_sent.values()):
			
			attandance_data = attendances_data.get(curr_user_token)
			attendance_requirement = verifyAttendance(attandance_data, required_attendance, required_lectures)

			if (attendance_requirement):
				email_sent = sendEmail(template, curr_user_data, config.dry_run)
				# Add token to sent emails, this will be used later to update 
				if email_sent:
					logging.info("User " + curr_user_token + ": Email with certificate has been sent")
					certificate_sent_query = ("UPDATE iscritto SET attestato_inviato=1 WHERE token=%s")
					sent_emails = sent_emails + 1
					try:
						cursor.execute(certificate_sent_query, (curr_user_token,))
						cnx.commit()
					except Exception as e:
						print(e)
						logging.error("User " + curr_user_token + ": database could not be updated")
				else:
					logging.error("User " + curr_user_token + " email could not be sent")
			
			if (sent_emails >= config.email_limit):
				break
	
	cnx.close()

if __name__ == '__main__':
	main()
