function displayChapters(textTrack, videoElement) {
    if (textTrack) {
        textTrack.mode = 'hidden';
        for (var i = 0; i < textTrack.cues.length; ++i) {
            locationList = videoElement.closest('figure').querySelector(".chapters"),
                video = videoElement;
            console.dir(locationList);
            cue = textTrack.cues[i],
                chapterName = cue.text,
                start = cue.startTime,
                newLocale = document.createElement("li");
            var location = document.createElement("a");
            location.setAttribute('rel', start);
            location.setAttribute('id', start);
            location.setAttribute('tabindex', '0');
            var localeDescription = stringToHTML(chapterName);
            location.innerHTML = localeDescription;
            newLocale.appendChild(location);
            locationList.appendChild(newLocale);
            location.addEventListener("click",
                function() {
                    this.closest('figure').querySelector("video").currentTime = this.id;
                }, false);
        }
        textTrack.addEventListener("cuechange", function() {
            var currentLocation = this.activeCues[0].startTime,
                cueMatch = this.activeCues[0].text,
                matchingCueArray = document.querySelectorAll('[rel="' + currentLocation + '"]');
            for (var i = 0; i < matchingCueArray.length; ++i) {
                thisChapter = matchingCueArray[i];
                if (thisChapter.innerHTML == cueMatch) {
                    if (chapter = thisChapter) {
                        var locations = [].slice.call(chapter.closest('figure').querySelectorAll("figcaption .chapters li"));
                        var counter = 0;
                        for (var z = 0; z < locations.length; ++z) {
                            locations[z].classList.remove("currentli");
                            locations[z].querySelector('a').classList.remove("current");
                        }
                        chapter.parentNode.classList.add("currentli");
                        chapter.classList.add("current");

                        for (var x = 0; x < locations.length; ++x) {
                            if (locations[x].classList.contains("currentli")) {
                                counter++;
                            }
                            if (counter < 1) {
                                locations[x].classList.add("watched");
                            } else {
                                locations[x].classList.remove("watched");
                            }
                        }
                    }

                }
            }
        }, false);
    }
}

function setVideoFile() {
    // Get the Video element and create source and track child elements
    video = document.querySelector("#video-element")
    source = document.createElement("source")
    track_element = video.addTextTrack("captions", "Captions", "en") // will not contain actual tracks, only used for locating video
    source.src = player_parameters.video_file + ".mp4"
    video.appendChild(source)

    // Load the VTT parser
    vtt_parser = new window.WebVTTParser
    track_source = player_parameters.video_file + ".vtt"

    // Create HTTP requeste for VTT file
    track_request = fetch(track_source);

    // If request is successful parse the VTT file and display the chapters
    // Note: the parser that is used is a modified version of https://www.npmjs.com/package/webvtt-parser
    // where the returned object is a list of VTTCue objects instead of a list of dictionaries, this way 
    // it can be added directly to the track object
    track_request.then(req => {
        return req.text()
    }).then(response => {
        parsed_track = vtt_parser.parse(response, 'metadata');
        parsed_track.cues.forEach(element => {
            track_element.addCue(element);
        });
        console.log(parsed_track)
        displayChapters(track_element, video)
        video.load();
    })

    // Show an error in the console if request is unsuccessful
    track_request.catch(() => {
        console.log("Error loading VTT file, track list will not be shown")
        video.load();
    })

}

var support = (function() {
    if (!window.DOMParser) return false;
    var parser = new DOMParser();
    try {
        parser.parseFromString('x', 'text/html');
    } catch (err) {
        return false;
    }
    return true;
})();

var stringToHTML = function(str) {
    if (support) {
        var parser = new DOMParser();
        var doc = parser.parseFromString(str, 'text/html');
        return doc.body.innerHTML;
    }
    var dom = document.createElement('div');
    dom.innerHTML = str;
    return dom;
};

// Main function that sets video file and loads corresponding VTT
setVideoFile()