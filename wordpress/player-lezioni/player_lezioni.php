<?php
   /*
   Plugin Name: Player Lezioni
   Plugin URI: http://linux.studenti.polito.it
   description: Il plugin mostra il video indicato dallo shortcode
   Version: insert_commit_sha_here
   Author: Linux@studenti
   Author URI: http://linux.studenti.polito.it
   License: GPL2
   */	
   
	// Register and load styles and scripts for all the function we need
	function register_scripts_player_lezioni(){
		wp_register_script( 'player', plugin_dir_url( __FILE__ ) . '/assets/js/player.js' , '', '', true );
		wp_register_script( 'vtt_parser', plugin_dir_url( __FILE__ ) . '/assets/js/vtt_parser.js' , '', '', true );
		wp_register_style( 'player', plugin_dir_url( __FILE__ ) . "/assets/css/player.css", array(), null, 'all' );
	}
	// Define the SPECIFIC styles and scripts we need to enqueue for the shortcode to work, for example
	// we don't necessarily need to load leaflet when we call the lab list

	function enqueue_scripts_player(){
		wp_enqueue_script('vtt_parser');
		wp_enqueue_script('player');
		wp_enqueue_style('player');
	}
	
	add_action( 'wp_enqueue_scripts', 'register_scripts_player_lezioni');

	// Define shortcode for wordpress player
	function wp_player_shortcode( $atts ){
		// Enqueue scripts only when shortcode is called
		enqueue_scripts_player();
		wp_localize_script( 'player', 'player_parameters', $atts);
		// Return map
		// Return listascuole
		ob_start();
		include('player.html');
		return ob_get_clean();;
	}	
	
	// Add shortcode for map
	add_shortcode('player_lezioni', 'wp_player_shortcode');

