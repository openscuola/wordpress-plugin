<?php

// Start user session
session_start();

$_SESSION['number1'] = rand(1, 20);
$_SESSION['number2'] = rand(1, 20);


// Send session code
header('Content-Type: application/json');
$session_data = array('number1' => $_SESSION['number1'], 'number2' => $_SESSION['number2']);
print json_encode($session_data);

?>