<?php
   /*
   Plugin Name: Gestione Corsi
   Plugin URI: http://linux.studenti.polito.it
   description: Il plugin permette di gestire le iscrizioni ai corsi di Linux@studenti.
   Version: insert_commit_sha_here
   Author: Linux@studenti
   Author URI: http://linux.studenti.polito.it
   License: GPL2
   */	
   
	// Register and load styles and scripts for all the function we need
	function iscrizione_corsi_reg_scripts(){
		// Load JQuery for querying the database, bootstrap, leaflet and marker cluster to load the map, mappa_osc to generate the map
		wp_register_script( 'submit_js', plugin_dir_url( __FILE__ ) .'/assets/js/submit.js' , '', '', true );
		wp_register_script( 'manage_js', plugin_dir_url( __FILE__ ) .'/assets/js/manage.js' , '', '', true );
		wp_register_script( 'vue_js', plugin_dir_url( __FILE__ ) .'/assets/js/vue.js' , '', '', true );
		wp_register_style( 'main_css', plugin_dir_url( __FILE__ ) . "/assets/css/main.css", array(), null, 'all' );
	}

	function enqueue_scripts_form(){
		wp_enqueue_script('submit_js');
		wp_localize_script( 'submit_js', 'iscrizione_corsi_vars', array(
			'base_dir' => ( plugin_dir_url( __FILE__ ) ),
		) );
		wp_enqueue_style('main_css');
	}

	function enqueue_scripts_manage(){
		wp_enqueue_script('vue_js');
		wp_enqueue_script('manage_js');
		wp_enqueue_style('main_css');
	}


	// Define the shortcode for the results summary

	function wp_iscrizioni_shortcode( $atts ){
		enqueue_scripts_form();
		// Return the div where the results will be loaded
		include('pages/iscrizione.html');
		return ob_get_clean();
	}

	function wp_course_manage_shortcode( $atts ){
		enqueue_scripts_manage();
		wp_localize_script( 'manage_js', 'iscrizione_corsi_vars', array(
			'base_dir' => ( plugin_dir_url( __FILE__ ) ),
			'token' => $_GET['token']
		) );
		// Return the div where the manager page will be loaded
		include('pages/gestione_iscrizione.html');
		return ob_get_clean();
	}
	
	
	// Add shortcode for form
	add_shortcode('iscrizione_corsi', 'wp_iscrizioni_shortcode');
	add_shortcode('gestione_utente', 'wp_course_manage_shortcode');
	add_action( 'wp_enqueue_scripts', 'iscrizione_corsi_reg_scripts');
