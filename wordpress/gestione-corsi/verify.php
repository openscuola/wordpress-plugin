<?php

$config_file='config/config.ini';
$config_parsed=parse_ini_file($config_file);

$servername = $config_parsed['servername'];
$username = $config_parsed['username']; // change with correct username and password
$password = $config_parsed['password'];
$dbname = $config_parsed['dbname'];
$charset = $config_parsed['charset'];
$base_url  = $config_parsed['base_url'];

$dsn = "mysql:host=$servername;dbname=$dbname;charset=$charset";
$options = array(
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
);
try {
    $db = new PDO($dsn, $username, $password, $options);
} catch (\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int)$e->getCode());
}

$user_query = $db->prepare("SELECT confermato, token FROM iscritto WHERE token = ?");
$user_rs= $user_query->execute([$_GET['token']]);
if (!$user_rs){
    return -1;
}
$token_rs= $user_query->fetch(PDO::FETCH_ASSOC);

if($token_rs['confermato'] == 0 && $token_rs['token'] == $_GET['token'])
{
    $token_status_update = "UPDATE iscritto SET confermato=? WHERE token=?";
    $db->prepare($token_status_update)->execute([1, $_GET['token']]);
    echo "<h1>Iscrizione completata</h1> <h2>L'iscrizione è stata completata con successo</h2>";
}
else if ($token_rs['confermato'] == 1){
    echo "<h1>Iscrizione già effettuata</h1> <h2>Risulta attualmente un'iscrizione attiva associata a questo link</h2>";
}
else{
    echo "<h1>Errore</h1> <h2>Il link non è valido</h2>";
}

?>
    