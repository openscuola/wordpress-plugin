<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require './PHPMailer/src/Exception.php';
require './PHPMailer/src/PHPMailer.php';
require './PHPMailer/src/SMTP.php';

$mail = new PHPMailer(true);

$config_file='config/config.ini';
$config_parsed=parse_ini_file($config_file);

$servername = $config_parsed['servername'];
$username = $config_parsed['username']; // change with correct username and password
$password = $config_parsed['password'];
$dbname = $config_parsed['dbname'];
$charset = $config_parsed['charset'];
$base_url  = $config_parsed['base_url'];

$username_email = $config_parsed['username_email'];
$password_email = $config_parsed['password_email'];
$address_email = $config_parsed['address_email'];
$mail_server = $config_parsed['mail_server'];

$dsn = "mysql:host=$servername;dbname=$dbname;charset=$charset";
$options = array(
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
);
try {
    $db = new PDO($dsn, $username, $password, $options);
} catch (\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int)$e->getCode());
}

session_start();

// Useful URLS
$GLOBALS['verify_url'] = $base_url . "/wp-content/plugins/gestione-corsi/verify.php";
$GLOBALS['manager_url'] = $base_url . "/gestione-utente-corso";

// Set up mail parameters
$mail->SMTPDebug = 0;                      // 4 to enable verbose debug output
$mail->isSMTP();                                            //Send using SMTP
$mail->Host       = $mail_server;                     //Set the SMTP server to send through
$mail->SMTPAuth   = true;                                   //Enable SMTP authentication
$mail->Username   = $username_email;                     //SMTP username
$mail->Password   = $password_email;                             //SMTP password
$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
$mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

//Recipients
$mail->setFrom("$address_email", 'linux@studenti');

$is_resend = array_key_exists("resend", $_GET);

if ($is_resend){
    $user_email = $_GET['email'];
    $response = resendEmail($user_email, $mail, $db);
}
else{
    $user_email = $_POST['email'];
    $response = registerNewUser($user_email, $mail, $db);
}

exit(json_encode($response));

function resendEmail($user_email, $mail, $db){
    $response['status_code'] = 0;
    $token_query = $db->prepare("SELECT nome, cognome, token FROM iscritto WHERE email = ?");
    $token_rs= $token_query->execute([$user_email]);

    if (!$token_rs){
        $response['status_code'] = 6;
        return $response;
    }
    
    $user_info = $token_query->fetch(PDO::FETCH_ASSOC);

    if (isset($user_info['token'])){
        $mail->addAddress($user_email);     //Add a recipient
        $mail->isHTML(true); //Set email format to HTML
        $mail->Subject = 'linux@studenti - Re-invio link gestione ' . $course_name["nome_corso"];
        $mail->Body    = 'Gentile ' . $user_info["nome"] . " " . $user_info["cognome"] . "<br> Come da lei richiesto le re-inviamo il link per poter accedere al registro presenze
        e gestire la sua iscrizione. <br>
        Per gestire il tuo account utilizza questo link  <a href=
        " . $GLOBALS['manager_url'] . "?token=" . $user_info['token'] . ">questa pagina</a>. <br>";
        try {
            $mail->send();
        }
        catch (Exception $e) {
            // Caught SMTP error
            $response['status_code'] = 4;        
    }
    }
    else {
        $response['status_code'] = 1;
    }

    return $response;
    
}

function registerNewUser($user_email, $mail, $db){
    // Check Captcha
    if ($_POST['captcha'] == $_SESSION['number1'] + $_SESSION['number2']){
        
        if ($_POST['data_policy'] != "on"){
            exit("Data Policy non accettata\n");
        }

        // Obtain course name from DB
        $courses_query = $db->prepare("SELECT nome_corso FROM corso WHERE id_corso = ?");
        $courses_rs= $courses_query->execute([$_POST['nome_corso']]);
        if (!$courses_rs){
            exit("An SQL error occured.\n");
        }
        $course_name = $courses_query->fetch(PDO::FETCH_ASSOC);

        if(!isset($course_name['nome_corso']))
        {
            $response['status_code'] = 1;
            exit(json_encode($response));
        }
        else {
            try {
                //Server settings
        
                $token = bin2hex(random_bytes(16));
        
                $registration_status = registerUser($db, $_POST, $token);
                $response['status_code'] = $registration_status;
        
                // Send email if resending token or if registration is completed
                if($registration_status == 0 || $registration_status == 3){
                    //Content
                    $mail->addAddress($user_email);     //Add a recipient
                    $mail->isHTML(true); //Set email format to HTML
                    $mail->Subject = 'linux@studenti - Iscrizione ' . $course_name["nome_corso"];
                    $mail->Body    = 'Gentile ' . $_POST["nome"] . " " . $_POST["cognome"] . "<br> la richiesta di iscrizione
                    al " . $course_name["nome_corso"]. " &egrave; stata correttamente ricevuta. <br>Per confermare la tua iscrizione e gestire il tuo account
                    utilizza <a href= " . $GLOBALS['manager_url'] . "?token=" . $token . ">questo link</a>. <br>";

                    $mail->send();            
                }
            } catch (Exception $e) {
                // Caught SMTP error
                $response['status_code'] = 4;        
            }
        }  
    }
    // Captcha invalid
    else{
        $response['status_code'] = 5;
    }
    return $response;
}

// Returns registration status, status codes are the same as the ones from the mainn plugin
function registerUser($db, $info, $token){
    $user_query = $db->prepare("SELECT confermato FROM iscritto WHERE email = ? AND id_corso = ?");
    $user_rs= $user_query->execute([$info['email'], $info["nome_corso"]]);
    if (!$user_rs){
        return -1;
    }
    $users_rs= $user_query->fetch(PDO::FETCH_ASSOC);
	
	$timestamp = date("Y-m-d H:i:s");;

    if(!isset($users_rs['confermato']))
    {
        $user_insert='INSERT INTO iscritto(nome, cognome, email, matricola, telefono, id_corso, token, confermato, timestamp, attestato_inviato) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        $db->prepare($user_insert)->execute([$info['nome'], $info['cognome'], $info['email'], $info['matricola'], $info['phone_number'], $info['nome_corso'], $token, 0, $timestamp, 0]);
        return 0;
    }
    else if($users_rs['confermato'] == 1){
        return 2;
    }
    // If not confirmed update token
    else if($users_rs['confermato'] == 0){
        $ip_insert='UPDATE iscritto SET token = ? WHERE email = ? AND id_corso = ? ';
        $db->prepare($ip_insert)->execute([$token, $info['email'], $info['nome_corso']]);
        return 3;
    }
    else{
        return -1;
    }
}

function checkAddr($remote_addr, $db)
{
    //$ip_query='SELECT tries FROM ip_log WHERE ip_address="' . $remote_addr . '";';
	$ip_query = $db->prepare("SELECT tries FROM ip_log WHERE ip_address=?");
    $ip_rs= $ip_query->execute([$remote_addr]);
	if (!$ip_rs){
		exit("An SQL error occured.\n");
	}
	$ip_rs= $ip_query->fetch(PDO::FETCH_ASSOC);

    if(!isset($ip_rs['tries']))
    {
        $ip_insert='INSERT INTO `ip_log`(ip_address, tries, timestamp) VALUES (?, ?, ?)';
        $db->prepare($ip_insert)->execute([$remote_addr, 1, time()]);
        return true;
    }
    else
    {
        $tries = intval($ip_rs['tries']);
        if($tries > 3){
            return false; 
        }
        else{
            $ip_update = "UPDATE ip_log SET tries=? WHERE ip_address=?";
            $db->prepare($ip_update)->execute([$tries + 1, $remote_addr]);
            return true;
        }
    }
}

?>


