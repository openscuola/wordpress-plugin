<?php

    $config_file='config/config.ini';
    $config_parsed=parse_ini_file($config_file);

    $servername = $config_parsed['servername'];
    $username = $config_parsed['username']; // change with correct username and password
    $password = $config_parsed['password'];
    $dbname = $config_parsed['dbname'];
    $charset = $config_parsed['charset'];

    $dsn = "mysql:host=$servername;dbname=$dbname;charset=$charset";
    $options = array(
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    );
    try {
        $db = new PDO($dsn, $username, $password, $options);
    } catch (\PDOException $e) {
        throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }

    $today= date("Y-m-d");
    $courses_query = $db->prepare("SELECT nome_corso, id_corso, inizio_iscrizioni, fine_iscrizioni FROM corso WHERE ? >= inizio_iscrizioni AND ? <= fine_iscrizioni");
    $courses_rs= $courses_query->execute([$today, $today]);
    if (!$courses_rs){
        exit("An SQL error occured.\n");
    }
    while($courses_r = $courses_query->fetch(PDO::FETCH_ASSOC)) {
        $response[] = $courses_r;
    }

    header('Content-Type: application/json');
    print json_encode($response);

?>