<?php

    $config_file='config/config.ini';
    $config_parsed=parse_ini_file($config_file);

    $servername = $config_parsed['servername'];
    $username = $config_parsed['username']; // change with correct username and password
    $password = $config_parsed['password'];
    $dbname = $config_parsed['dbname'];
    $charset = $config_parsed['charset'];

    $user_token = $_GET['token'];
    $attendance_password = $_GET['attendance_password'];

    // Connect to DB

    $dsn = "mysql:host=$servername;dbname=$dbname;charset=$charset";
    $options = array(
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    );
    try {
        $db = new PDO($dsn, $username, $password, $options);
    } catch (\PDOException $e) {
        throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }

    $return['status_code'] = 0;

    // Obtain lesson number and course

    $today= date("Y-m-d");
    $lecture_query = $db->prepare("SELECT id_corso, numero_lezione FROM lezione WHERE password = ?");
    $lecture_rs= $lecture_query->execute([$attendance_password]);
    if (!$lecture_rs){
        die("An SQL error occured.\n");
    }
    $lecture_info = $lecture_query->fetch(PDO::FETCH_ASSOC);
    // couldn't find password
    if (!$lecture_info){
        $return['status_code'] = 1;
    }
    else {
        $user_query = $db->prepare("SELECT confermato FROM iscritto WHERE token = ?");
        $user_rs= $user_query->execute([$user_token]);
        $user_data = $user_query->fetch(PDO::FETCH_ASSOC);
        if (!$user_rs){
            die("An SQL error occured.\n");
        }
        else if (!$user_data){
            $return['status_code'] = 2;
        }
        else if ($user_data['confermato'] == 0){
            $return['status_code'] = 3;
        }
        else {
            $attendance_query = $db->prepare("INSERT INTO presenza (token, password, id_corso) VALUES (?, ?, ?);");
            try {
                $attendance_rs= $attendance_query->execute([$user_token, $attendance_password, $lecture_info['id_corso']]);
            }
            catch ( PDOException $e) {
                // Integrity constraint violation -> trying to submit attendance twice
                if ($e->getCode() == 23000){
                    $return['status_code'] = 5;
                }
            }
        }

    } 
    exit(json_encode($return))

?>