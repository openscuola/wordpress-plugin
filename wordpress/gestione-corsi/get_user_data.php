<?php

    $config_file='config/config.ini';
    $config_parsed=parse_ini_file($config_file);

    $servername = $config_parsed['servername'];
    $username = $config_parsed['username']; // change with correct username and password
    $password = $config_parsed['password'];
    $dbname = $config_parsed['dbname'];
    $charset = $config_parsed['charset'];

    $user_token = $_GET['token'];

    $course_info_cache = array();

    $dsn = "mysql:host=$servername;dbname=$dbname;charset=$charset";
    $options = array(
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    );
    try {
        $db = new PDO($dsn, $username, $password, $options);
    } catch (\PDOException $e) {
        throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }

    $courses_query = $db->prepare("SELECT nome, cognome, email, matricola, telefono, id_corso, token, confermato, attestato_inviato FROM iscritto WHERE token = ?");
    $courses_rs= $courses_query->execute([$user_token]);
    if (!$courses_rs){
        exit("An SQL error occured.\n");
    }
    $response['user_data'] = $courses_query->fetch(PDO::FETCH_ASSOC);

    $attandance_query = $db->prepare("SELECT presenza.id_corso, lezione.numero_lezione FROM presenza INNER JOIN lezione ON presenza.password = lezione.password WHERE presenza.token = ? ORDER BY lezione.numero_lezione");
    $attandance_rs= $attandance_query->execute([$user_token]);
    if (!$attandance_rs){
        exit("An SQL error occured.\n");
    }

    if ($response['user_data']){
        $course_info = getCourseInfo($response['user_data']['id_corso'], $course_info_cache, $db);
        $response['user_data']['nome_corso'] = $course_info['nome_corso'];
        $response['user_data']['presenze_obbligatorie'] = $course_info['presenze_obbligatorie'];
        $response['user_data']['lezioni_obbligatorie'] = $course_info['lezioni_obbligatorie'];
    }
    
    while ($r = $attandance_query->fetch(PDO::FETCH_ASSOC)){
        // Query for course name
        $course_info = getCourseInfo($r['id_corso'], $course_info_cache, $db);
        $curr_attendance['nome_corso'] = $course_info['nome_corso'];
        $curr_attendance['numero_lezione'] = $r['numero_lezione'];
        $response['attendance_data'][] = $curr_attendance;
    }

    header('Content-Type: application/json');
    print json_encode($response);


function getCourseInfo($course_id, $course_cache, $db)
{
    if (!array_key_exists($course_id, $course_cache)){
        $course_query = $db->prepare("SELECT nome_corso, presenze_obbligatorie FROM corso WHERE id_corso = ?");
        $course_rs= $course_query->execute([$course_id]);
        if (!$course_rs){
            return " ";
        }
        $course_data = $course_query->fetch(PDO::FETCH_ASSOC);

        $required_lectures_query = $db->prepare("SELECT numero_lezione, lezione_obbligatoria FROM lezione WHERE id_corso = ?");
        $required_lectures_rs= $required_lectures_query->execute([$course_id]);
        if (!$required_lectures_rs){
            return " ";
        }
        while ($r = $required_lectures_query->fetch(PDO::FETCH_ASSOC)){
            if ($r['lezione_obbligatoria'] > 0){
                $course_data['lezioni_obbligatorie'][] = $r['numero_lezione'];
            }
        }

        $course_cache[$course_id] = $course_data;
    }
    return $course_cache[$course_id];
}

function getCertificateStatus($attendance_data, $course_info){
    return 0;
}

?>