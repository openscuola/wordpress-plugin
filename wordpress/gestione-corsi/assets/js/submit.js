document.querySelector("body").onload = function(){
	// Get data from DB
	console.log("body loaded");
	course_data_req = fetch(iscrizione_corsi_vars.base_dir + "get_data.php");
	course_data_req.then((response => {
		return response.json()
	}))
	.then(data => {
		if (data != null){
			for (const corso of data) {
				course_option = document.createElement('option');
				course_option.value = corso['id_corso'];
				course_option.text = corso['nome_corso'];
				document.querySelector('#nome_corso').append(course_option)
			}
			document.querySelector("#form-iscrizione").style.display = 'block';
			document.querySelector("#no-courses").style.display = 'none';
			// Ask for session's security question
			setCaptcha();
		}
		else{
			document.querySelector("#form-iscrizione").style.display = 'none';
			document.querySelector("#no-courses").style.display = 'block';
		}
	});
};

document.querySelector("#form-iscrizione").addEventListener('submit', (event) => {
	event.preventDefault();

	toggleLoader(true);

    // Bind the FormData object and the form element
    const formData = new FormData(document.querySelector("#form-iscrizione"));

    const request = new Request(iscrizione_corsi_vars.base_dir + "submit.php", {
		method: 'POST',
		body: formData
	  });

	request_fetch = fetch(request)
	  
	request_fetch.then((response) => {
		return response.json();
	})
	.then((data) => {
		toggleLoader(false);
		handleError(data["status_code"]);
	});

});

function handleError(error){
	document.querySelector("#form-iscrizione").style.display = 'none';
	switch(error){
		case 0:
			document.querySelector("#submit-success").style.display = 'block';
			break;
		case 2:
			document.querySelector("#submit-existing").style.display = 'block';
			break;
		case 3:
			document.querySelector("#submit-resend").style.display = 'block';
			break;
		default:
			document.querySelector("#submit-internal-error").style.display = 'block';
	}
	
}

function toggleLoader(load){
	if (load){
		document.querySelector("#submit").style.display = 'none';
		document.querySelector(".loader").style.display = 'block';
	}
	else {
		document.querySelector("#submit").style.display = 'block';
		document.querySelector(".loader").style.display = 'none';
	}
}

function setCaptcha(){
	captcha_request = fetch(iscrizione_corsi_vars.base_dir + "get_captcha.php");
	captcha_request.then(response => {
		return response.json()
	})
	.then(data => {
			document.querySelector("#number1").text = data['number1'];
			document.querySelector("#number2").text = data['number2'];
	})
}