class User {
    constructor(user_data) {
        if (user_data != null && user_data != false){
            this.nome = user_data.nome;
            this.cognome = user_data.cognome;
            this.email = user_data.email
            this.matricola = user_data.matricola;
            this.telefono = user_data.telefono;
            this.id_corso = user_data.id_corso;
            this.nome_corso = user_data.nome_corso;
            this.confermato = user_data.confermato;
            this.attestato_inviato = user_data.attestato_inviato;
            this.lezioni_obbligatorie= user_data.lezioni_obbligatorie != null ? user_data.lezioni_obbligatorie : Array();
            this.presenze_obbligatorie = user_data.presenze_obbligatorie != null ? user_data.presenze_obbligatorie : 0;
            this.valid_user = true;
        } 
        else {
            this.nome = null;
            this.cognome = null;
            this.email = null;
            this.matricola = null;
            this.telefono = null;
            this.id_corso = null;
            this.nome_corso = null;
            this.confermato = null;
            this.attestato_inviato = null;
            this.lezioni_obbligatorie= null;
            this.presenze_obbligatorie = null;
            this.valid_user = false;
        }       
    }
}

class Attendance {
    constructor(attendance_data) {
        if (attendance_data != null){
            this.id_corso = attendance_data.id_corso;
            this.numero_lezione = attendance_data.numero_lezione;
            this.nome_corso = attendance_data.nome_corso;
        } 
        else {
            this.id_corso = null;
            this.numero_lezione = null;
            this.nome_corso = null;
        }       
    }
}

function removeTrailingWhiteSpace(src_string) {
    const re_trailing_whitespace = /\s*$/;
    return src_string.replace(re_trailing_whitespace, "");
}

var app = {
	el: '#gestione-iscrizione',
	data() {
		return {
			token: iscrizione_corsi_vars.token,
            error: 0,
            error_resend: -1,
            loaded: false,
            user_data: new User(null),
            stato_certificato: -1,
            attendance_data: [new Attendance(null)],
            attendance_password: "",
            resend_email: "",
            confirmed_first_time: 0,
		}
	},
	mounted() {
        this.loadUserData();
	},
	methods: {
		confirmSubscription: function () {
            params = new URLSearchParams({
                token: this.token
            }).toString();
            request = fetch(iscrizione_corsi_vars.base_dir + "/verify.php?" + params);

            response = request.then(() => {
                this.confirmed_first_time = 1;
            })
    
            response.catch((error) => {
                this.confirmed_first_time = 2;
            }) 

        },

        loadUserData: function (){
            params = new URLSearchParams({
                token: this.token
            }).toString();
            request = fetch(iscrizione_corsi_vars.base_dir + "/get_user_data.php?" + params);

            response = request.then(response => {
                return response.json();
            })
    
            response.then(data => {
                this.user_data = new User(data.user_data);
                if (this.user_data.valid_user){
                    if (!this.user_data.confermato){
                        this.confirmSubscription();
                    }
                    this.attendance_data = [];
                    if (data.attendance_data){
                        data.attendance_data.forEach(element => {
                            this.attendance_data.push(new Attendance(element));
                        });
                    }

                    this.updateCertificateStatus();
                }
                else {
                    this.error = 2;
                }
                this.loaded = true;

            })
    
            response.catch(() => {
                this.error = 4;
            })
        },

        submitPassword: function (){
            this.loaded = false;
            params = new URLSearchParams({
                token: this.token,
                attendance_password: removeTrailingWhiteSpace(this.attendance_password)
            }).toString();

            const request = new Request(iscrizione_corsi_vars.base_dir + "submit_attendance.php?" + params);

            request_fetch = fetch(request)
            
            request_fetch.then((response) => {
                return response.json();
            })
            .then((data) => {
                this.error = data["status_code"];
                if (!this.error){
                    this.attendance_password = "";
                }
                this.loadUserData();
            });
        },

        resendEmail: function(){
            params = new URLSearchParams({
                resend: true,
                email: this.resend_email,
            }).toString();
            this.loaded = false;
            const request = new Request(iscrizione_corsi_vars.base_dir + "submit.php?" + params);

            request_fetch = fetch(request)
            
            request_fetch.then((response) => {
                return response.json();
            })
            .then((data) => {
                this.error_resend = data["status_code"];
                this.loaded = true;
            });
        },

        checkRequiredLecture: function(){
            if (this.user_data.lezioni_obbligatorie.length == 0){
                return true
            }
            for (curr_lecture of this.user_data.lezioni_obbligatorie){
                found_lecture = false
                for (curr_attendance of this.attendance_data){
                    if (curr_lecture == curr_attendance.numero_lezione){
                        found_lecture = true
                    }
                }
                if (!found_lecture){
                    return false
                }
            }
            return true
        },

        updateCertificateStatus: function(){
            if (this.user_data.attestato_inviato){
                this.stato_certificato = 0
            }
            else {
                console.log(this.checkRequiredLecture())
                console.log(this.attendance_data.length)
                console.log(this.user_data.presenze_obbligatorie)
                if (this.checkRequiredLecture() && (this.attendance_data.length >= this.user_data.presenze_obbligatorie) ){
                    this.stato_certificato = 1
                }
                else if (this.checkRequiredLecture() && !(this.attendance_data.length >= this.user_data.presenze_obbligatorie)){
                    this.stato_certificato =  2
                }
                else if (!this.checkRequiredLecture() && (this.attendance_data.length >= this.user_data.presenze_obbligatorie)){
                    this.stato_certificato =  3
                }
                else{
                    this.stato_certificato = 4
                }
            }
        }

	}
}

Vue.createApp(app).mount('#gestione-iscrizione')
