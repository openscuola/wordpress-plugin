<?php
   /*
   Plugin Name: OpenScuola
   Plugin URI: http://linux.studenti.polito.it
   description: Il plugin effettua una query al db di openscuola e visualizza le informazioni su una pagina wordpress.
   Version: insert_commit_sha_here
   Author: Linux@studenti
   Author URI: http://linux.studenti.polito.it
   License: GPL2
   */	
   
	// Register and load styles and scripts for all the function we need
	function register_scripts(){
		// Load JQuery for querying the database, bootstrap, leaflet and marker cluster to load the map, mappa_osc to generate the map
		wp_register_script( 'jquery', plugin_dir_url( __FILE__ ) . '/assets/js/jquery.min.js' , '', '', true );
		wp_register_script( 'axios', plugin_dir_url( __FILE__ ) . '/assets/js/axios.js' , '', '', true );
		wp_register_script( 'bootstrap', plugin_dir_url( __FILE__ ) .'/assets/bootstrap/js/bootstrap.min.js' , '', '', true );
		wp_register_script( 'leaflet',plugin_dir_url( __FILE__ ) . '/assets/leaflet/leaflet.js' , '', '', true );
		wp_register_script( 'markercluster', plugin_dir_url( __FILE__ ) .'/assets/leaflet/plugins/leaflet.markercluster/leaflet.markercluster.js' , '', '', true );
		wp_register_script( 'mappa_osc', plugin_dir_url( __FILE__ ) .'/assets/js/mappa_osc.js' , '', '', true );
		wp_register_script( 'lista_osc', plugin_dir_url( __FILE__ ) .'/assets/js/lista_osc.js' , '', '', true );
		wp_register_script( 'institute_osc', plugin_dir_url( __FILE__ ) .'/assets/js/institute_osc.js' , '', '', true );
		wp_register_script( 'getInstitute_osc', plugin_dir_url( __FILE__ ) .'/assets/js/getInstitute_osc.js' , '', '', true );
		wp_register_script( 'getComplex_osc', plugin_dir_url( __FILE__ ) .'/assets/js/getComplex_osc.js' , '', '', true );
		wp_register_script( 'getLab_osc', plugin_dir_url( __FILE__ ) .'/assets/js/getLab_osc.js' , '', '', true );
		wp_register_script( 'getPC_osc', plugin_dir_url( __FILE__ ) .'/assets/js/getPC_osc.js' , '', '', true );
		wp_register_script( 'getDonor_osc', plugin_dir_url( __FILE__ ) .'/assets/js/getDonor_osc.js' , '', '', true );
		wp_register_script( 'getLshw_osc', plugin_dir_url( __FILE__ ) .'/assets/js/getLshw_osc.js' , '', '', true );
		wp_register_script( 'getImages_osc', plugin_dir_url( __FILE__ ) .'/assets/js/getImages.js' , '', '', true );
		wp_register_script( 'openscuola_util', plugin_dir_url( __FILE__ ) .'/assets/js/openscuola_util.js' , '', '', true );
		wp_register_script( 'getResults_osc', plugin_dir_url( __FILE__ ) .'/assets/js/getResults.js' , '', '', true );
		wp_register_script( 'getSwitch_osc', plugin_dir_url( __FILE__ ) .'/assets/js/getSwitch.js' , '', '', true );
		wp_register_script( 'getStatistics_osc', plugin_dir_url( __FILE__ ) .'/assets/js/getStatistics.js' , '', '', true );
		wp_register_script( 'd3js', plugin_dir_url( __FILE__ ) . '/assets/js/d3.v5.min.js' , '', '', true );
		wp_register_script('d3tip', plugin_dir_url( __FILE__ ) . '/assets/js/d3tip.js', '', '', true);
		wp_register_script( 'turf', plugin_dir_url( __FILE__ ) . '/assets/js/turf.min.js' , '', '', true );
		wp_register_script( 'vue.js', plugin_dir_url( __FILE__ ) . '/assets/js/vue.js' , '', '', true );
		// Script for gallery
		wp_register_script( 'gallery',  plugin_dir_url( __FILE__ ) .'/assets/js/gallery.js', '', '', true );
		// Script for touch gestures
		wp_register_script( 'hammer',  plugin_dir_url( __FILE__ ) .'/assets/js/hammer.min.js', '', '', true );
		wp_register_style( 'bootstrap',  plugin_dir_url( __FILE__ ) . '/assets/bootstrap/css/bootstrap.css', '', '', true );
		wp_register_style( 'leaflet',  plugin_dir_url( __FILE__ ) . '/assets/leaflet/leaflet.css', array(), null, 'all' );
		wp_register_style( 'leaflet.markercluster',  plugin_dir_url( __FILE__ ) . '/assets/leaflet/plugins/leaflet.markercluster/MarkerCluster.css', array(), null, 'all' );
		wp_register_style( 'default_marker',  plugin_dir_url( __FILE__ ) . '/assets/leaflet/plugins/leaflet.markercluster/default_marker.css', array(), null, 'all' );
		wp_register_style( 'norican', plugin_dir_url( __FILE__ ) . '/assets/css/norican.css' , array(), null, 'all' );
		wp_register_style( 'main', plugin_dir_url( __FILE__ ) . "/assets/css/main.css", array(), null, 'all' );
		// Style for gallery
		wp_register_style('gallery', plugin_dir_url( __FILE__ ) .'/assets/css/gallery.css' , array(), null, 'all' );
	}
	// Define the SPECIFIC styles and scripts we need to enqueue for the shortcode to work, for example
	// we don't necessarily need to load leaflet when we call the lab list

	function enqueue_scripts_results(){
		wp_enqueue_script('getResults_osc');
		wp_localize_script( 'getResults_osc', 'getResults_osc_vars', array(
			'base_dir' => ( plugin_dir_url( __FILE__ ) ),
		) );
		wp_enqueue_script('d3js');
		wp_enqueue_script('d3tip');
		wp_enqueue_script('turf');
		wp_enqueue_style('main');
	}

	function enqueue_scripts_map(){
		wp_enqueue_style('leaflet');
		wp_enqueue_style('leaflet.markercluster');
		wp_enqueue_style('default_marker');
		wp_enqueue_style('norican');
		// Some custom CSS to properly style some elements 
		wp_enqueue_style('main');
		wp_enqueue_script('jquery');
		wp_enqueue_script('bootstrap');
		wp_enqueue_script('leaflet');
		wp_enqueue_script('markercluster');
		wp_enqueue_script('openscuola_util');
		wp_enqueue_script('mappa_osc');
		wp_localize_script( 'mappa_osc', 'mappa_osc_vars', array(
			'base_dir' => ( plugin_dir_url( __FILE__ ) ),
		) );
	}
	
	function enqueue_scripts_list(){
		wp_enqueue_script('vue.js');
		wp_enqueue_script('axios');
		wp_enqueue_script('lista_osc');
		wp_enqueue_script('openscuola_util');
		wp_enqueue_style('main');
		wp_localize_script( 'lista_osc', 'lista_osc_vars', array(
			'base_dir' => ( plugin_dir_url( __FILE__ ) ),
		) );
	}
	
	function enqueue_scripts_institute(){
		wp_enqueue_script('jquery');
		wp_enqueue_script('bootstrap');
		wp_enqueue_script('institute_osc');
		wp_enqueue_script('openscuola_util');
		wp_enqueue_style('main');
		wp_localize_script( 'institute_osc', 'institute_osc_vars', array(
			'base_dir' => ( plugin_dir_url( __FILE__ ) ),
		) );
	}
	
	// Functions needed to generate the graph
	
	function enqueue_scripts_getInstitute(){
		wp_enqueue_script('vue.js');
		wp_enqueue_script('axios');
		wp_enqueue_script('bootstrap');
		wp_enqueue_script('leaflet');
		wp_enqueue_script('markercluster');
		wp_enqueue_script('getInstitute_osc');
		wp_enqueue_script('openscuola_util');
		wp_enqueue_style('main');
		wp_enqueue_style('leaflet');
		wp_enqueue_style('leaflet.markercluster');
		wp_enqueue_style('default_marker');
		wp_localize_script( 'getInstitute_osc', 'getInstitute_osc_vars', array(
			'base_dir' => ( plugin_dir_url( __FILE__ ) ),
			'host_dir' => ( get_site_url() ),
		) );
	}
	
	function enqueue_scripts_getComplex(){
		wp_enqueue_script('vue.js');
		wp_enqueue_script('axios');
		wp_enqueue_script('bootstrap');
		wp_enqueue_script('leaflet');
		wp_enqueue_script('markercluster');
		wp_enqueue_script('getComplex_osc');
		wp_enqueue_script('openscuola_util');
		wp_enqueue_style('main');
		wp_enqueue_style('leaflet');
		wp_enqueue_style('leaflet.markercluster');
		wp_enqueue_style('default_marker');
		wp_localize_script( 'getComplex_osc', 'getComplex_osc_vars', array(
			'base_dir' => ( plugin_dir_url( __FILE__ ) ),
		) );
	}
	
	function enqueue_scripts_getLab(){
		wp_enqueue_script('axios');
		wp_enqueue_script('vue.js');
		wp_enqueue_script('bootstrap');
		wp_enqueue_script('getLab_osc');
		wp_enqueue_script('openscuola_util');
		wp_enqueue_script('hammer');
		wp_enqueue_script('gallery');
		wp_enqueue_style('main');
		wp_enqueue_style('gallery');
		wp_localize_script( 'getLab_osc', 'getLab_osc_vars', array(
			'base_dir' => ( plugin_dir_url( __FILE__ ) ),
			'host_dir' => ( get_site_url() ),
		) );
	}
	
	function enqueue_scripts_getPC(){
		wp_enqueue_script('axios');
		wp_enqueue_script('vue.js');
		wp_enqueue_script('bootstrap');
		wp_enqueue_script('getPC_osc');
		wp_enqueue_script('openscuola_util');
		wp_enqueue_style('main');
		wp_localize_script( 'getPC_osc', 'getPC_osc_vars', array(
			'base_dir' => ( plugin_dir_url( __FILE__ ) ),
			'host_dir' => ( get_site_url() ),
		) );
	}

	function enqueue_scripts_getSwitch(){
		wp_enqueue_script('axios');
		wp_enqueue_script('vue.js');
		wp_enqueue_script('getSwitch_osc');
		wp_enqueue_script('openscuola_util');
		wp_enqueue_style('main');
		wp_localize_script( 'getSwitch_osc', 'getSwitch_osc_vars', array(
			'base_dir' => ( plugin_dir_url( __FILE__ ) ),
			'host_dir' => ( get_site_url() ),
		) );
	}

	function enqueue_scripts_getDonor(){
		wp_enqueue_script('axios');
		wp_enqueue_script('vue.js');
		wp_enqueue_script('getDonor_osc');
		wp_enqueue_script('openscuola_util');
		wp_enqueue_style('main');
		wp_localize_script( 'getDonor_osc', 'getDonor_osc_vars', array(
			'base_dir' => ( plugin_dir_url( __FILE__ ) ),
			'host_dir' => ( get_site_url() ),
		) );
	}
	function enqueue_scripts_getLshw(){
		wp_enqueue_script('jquery');
		wp_enqueue_script('bootstrap');
		wp_enqueue_script('openscuola_util');
		wp_enqueue_script('getLshw_osc');
		wp_enqueue_style('main');
		wp_localize_script( 'getLshw_osc', 'getLshw_osc_vars', array(
			'base_dir' => ( plugin_dir_url( __FILE__ ) ),
			'host_dir' => ( get_site_url() ),
		) );
	}
	function enqueue_scripts_getImages(){
		wp_enqueue_script('jquery');
		wp_enqueue_script('bootstrap');
		wp_enqueue_script('getImages_osc');
		wp_enqueue_script('openscuola_util');
		wp_enqueue_script('hammer');
		wp_enqueue_script('gallery');
		wp_enqueue_style('main');
		wp_enqueue_style('gallery');
		wp_localize_script( 'getImages_osc', 'getImages_osc_vars', array(
			'base_dir' => ( plugin_dir_url( __FILE__ ) ),
			'host_dir' => ( get_site_url() ),
		) );
	}
	function enqueue_scripts_getStatistics(){
		wp_enqueue_script('axios');
		wp_enqueue_script('getStatistics_osc');
		wp_localize_script( 'getStatistics_osc', 'getStatistics_osc_vars', array(
			'base_dir' => ( plugin_dir_url( __FILE__ ) ),
			'host_dir' => ( get_site_url() ),
		) );
	}
	add_action( 'wp_enqueue_scripts', 'register_scripts');


	// Define the shortcode for the results summary

	function wp_oscresults_shortcode ( $atts ){
		enqueue_scripts_results();
		// Return the div where the results will be loaded
		return '<div id="results"><div id="map-results"></div><div id="text-results"></div></div>';
	}
	
	// Define shortcode for map
	function wp_mappascuole_shortcode( $atts ){
		// Enqueue scripts only when shortcode is called
		enqueue_scripts_map();
		
		// Default value is empty, in that case mappa_osc.js will show everything
		$atts=shortcode_atts( array(
			'statuscode' => '',
		), $atts, 'mappascuole');
		// Load styles and scripts by calling defined action
		// Add action that calls functions loading scripts for the map
		
		// Check if we have the default value (empty)
		if (!$atts['statuscode']){
			// If we have empty input send "all" to mappa_osc.js, the script will arrange the map to show everything
			$statuscode_array = ( 'all' );
		}
		else {
			 // Sanitize data and remove whitespaces
		$no_whitespaces = preg_replace( '/\s*,\s*/', ',', filter_var( $atts['statuscode'], FILTER_SANITIZE_STRING ) ); 
		$statuscode_array = explode( ',', $no_whitespaces );
        	
		}

		if (!$_GET['center_lat'] && !$_GET['center_lon'] ){
			$center = '';
		}
		else {
			$center =  array( $_GET['center_lat'], $_GET['center_lon'] );
		}

		wp_localize_script( 'mappa_osc', 'mappa_osc_parameters', array(
			'show' => $statuscode_array,
			'center'  => $center,
		) );
		
		
		// Return map
		return '<div id="map" style="height:50em;"></div>';
	}	
	
	// Define shortcode for list
	function wp_listascuole_shortcode( $atts ){
		enqueue_scripts_list();
			// Check if we have the default value (empty)
		if (!$atts['statuscode']){
			// If we have empty input send "all" to lista_scuole.js, the script will arrange the list to show everything
			wp_localize_script( 'lista_osc', 'lista_osc_parameters', array(
			'show' => ( 'all' ),
			) );
			return '<div id="listascuole"></div>'; 
		}
		else {
			$no_whitespaces = preg_replace( '/\s*,\s*/', ',', filter_var( $atts['statuscode'], FILTER_SANITIZE_STRING ) ); 
			$statuscode_array = explode( ',', $no_whitespaces );
			wp_localize_script( 'lista_osc', 'lista_osc_parameters', array(
				'show' => $statuscode_array,
			) );
		}
		 // Sanitize data and remove whitespaces
		
		// Return listascuole
		ob_start();
		include('pages/lista_plessi.html');
		return ob_get_clean();;
	}
	// Define shortcode for complete institute list
	function wp_institute_osc_shortcode( $atts ){
		enqueue_scripts_institute();
		
		return '<div id="osc_institute"></div>';
	}
	// Define shortcode to get institute 
	function wp_getInstitute_osc_shortcode( $atts ){
		enqueue_scripts_getInstitute();
		$institute_id=$_GET['institute_id'];
		// Sanitize get input
		$no_whitespaces = preg_replace( '/\s*,\s*/', ',', filter_var( $institute_id, FILTER_SANITIZE_STRING ) ); 
		$institute_id_sanitized = explode( ',', $no_whitespaces );
        	wp_localize_script( 'getInstitute_osc', 'getInstitute_osc_parameters', array(
			'institute_id' => $institute_id_sanitized,
		) );
		ob_start();
		include('pages/istituto.html');
		return ob_get_clean();;
	}
	// Define shortcode to get complex 
	function wp_getComplex_osc_shortcode( $atts ){
		enqueue_scripts_getComplex();
		$complex_id=$_GET['complex_id'];
		// Sanitize get input
		$no_whitespaces = preg_replace( '/\s*,\s*/', ',', filter_var( $complex_id, FILTER_SANITIZE_STRING ) ); 
		$complex_id_sanitized = explode( ',', $no_whitespaces );
        	wp_localize_script( 'getComplex_osc', 'getComplex_osc_parameters', array(
			'complex_id' => $complex_id_sanitized,
		) );
		ob_start();
		include('pages/plesso.html');
		return ob_get_clean();;
	}
	// Define shortcode to get Lab 
	function wp_getLab_osc_shortcode( $atts ){
		enqueue_scripts_getLab();
		$lab_id=$_GET['lab_id'];
		// Sanitize get input
		$no_whitespaces = preg_replace( '/\s*,\s*/', ',', filter_var( $lab_id, FILTER_SANITIZE_STRING ) ); 
		$lab_id_sanitized = explode( ',', $no_whitespaces );
        	wp_localize_script( 'getLab_osc', 'getLab_osc_parameters', array(
			'lab_id' => $lab_id_sanitized,
		) );
		ob_start();
		include('pages/laboratorio.html');
		return ob_get_clean();;
	}
	function wp_getPC_osc_shortcode( $atts ){
		enqueue_scripts_getPC();
		$pc_id=$_GET['pc_id'];
		$pc_type=$_GET['pc_type'];
		// Sanitize get input
		$no_whitespaces = preg_replace( '/\s*,\s*/', ',', filter_var( $pc_id, FILTER_SANITIZE_STRING ) ); 
		$pc_id_sanitized = explode( ',', $no_whitespaces );
		$no_whitespaces = preg_replace( '/\s*,\s*/', ',', filter_var( $pc_type, FILTER_SANITIZE_STRING ) ); 
		$pc_type_sanitized = explode( ',', $no_whitespaces );
        	wp_localize_script( 'getPC_osc', 'getPC_osc_parameters', array(
			'pc_id' => $pc_id_sanitized,
			'pc_type' => $pc_type_sanitized,
		) );
		ob_start();
		include('pages/postazione.html');
		return ob_get_clean();
	}

	function wp_getSwitch_osc_shortcode( $atts ){
		enqueue_scripts_getSwitch();
		$switch_name=$_GET['switch_name'];
		// Sanitize get input
		$no_whitespaces = preg_replace( '/\s*,\s*/', ',', filter_var( $switch_name, FILTER_SANITIZE_STRING ) ); 
		$switch_name_sanitized = explode( ',', $no_whitespaces );
        	wp_localize_script( 'getSwitch_osc', 'getSwitch_osc_parameters', array(
			'switch_name' => $switch_name_sanitized,
		) );
		ob_start();
		include('pages/switch.html');
		return ob_get_clean();
	}

	function wp_getDonor_osc_shortcode( $atts ){
		enqueue_scripts_getDonor();
		$donor_id=$_GET['donor_id'];
		// Sanitize get input
		$no_whitespaces = preg_replace( '/\s*,\s*/', ',', filter_var( $donor_id, FILTER_SANITIZE_STRING ) ); 
		$donor_id_sanitized = explode( ',', $no_whitespaces );
        	wp_localize_script( 'getDonor_osc', 'getDonor_osc_parameters', array(
			'donor_id' => $donor_id_sanitized,
		) );
		ob_start();
		include('pages/donatore.html');
		return ob_get_clean();
	}
	function wp_getLshw_osc_shortcode( $atts ){
		enqueue_scripts_getLshw();
		$pc_id=$_GET['pc_id'];
		$pc_type=$_GET['pc_type'];
		// Sanitize get input
		$no_whitespaces = preg_replace( '/\s*,\s*/', ',', filter_var( $pc_id, FILTER_SANITIZE_STRING ) ); 
		$pc_id_sanitized = explode( ',', $no_whitespaces );
		$no_whitespaces = preg_replace( '/\s*,\s*/', ',', filter_var( $pc_type, FILTER_SANITIZE_STRING ) ); 
		$pc_type_sanitized = explode( ',', $no_whitespaces );
        wp_localize_script( 'getLshw_osc', 'getLshw_osc_parameters', array(
			'pc_id' => $pc_id_sanitized,
			'pc_type' => $pc_type_sanitized,
		) );
		return '<div id="osc_lshw"></div>';
	}
	
	function wp_createGallery_osc_shortcode( $atts ){
		enqueue_scripts_getImages();
		
		$atts=shortcode_atts( array(
			'gallery_id' => '',
		), $atts, 'createGallery');
		
		 // Sanitize data and remove whitespaces
		$no_whitespaces = preg_replace( '/\s*,\s*/', ',', filter_var( $atts['gallery_id'], FILTER_SANITIZE_STRING ) ); 
		
		$pc_id=$_GET['gallery_id'];
        wp_localize_script( 'getImages_osc', 'getImages_osc_parameters', array(
			'gallery_id' => $atts['gallery_id'],
		) );
		return '<div id="osc_gallery"></div>';
	}

	function wp_getStatistic_osc_shortcode( $atts ){
		enqueue_scripts_getStatistics();
		$element_id = uniqid(); // Get unique id for element, in case we have more than one stat
		$atts=shortcode_atts( array(
			'statistic' => '',
		), $atts, 'getStatistic');
		
		 // Sanitize data and remove whitespaces

		$statistic_clean=  preg_replace( '/\s*,\s*/', ',', filter_var( $atts['statistic'], FILTER_SANITIZE_STRING ) ); 

		// This cumbersome piece of code simply means create new object statistic with el_id uniqid() and statistic type $statistic_clean (ie. the sanitized statistics call)
		wp_add_inline_script( 'getStatistics_osc', 'statistic_' . $element_id . '= new Statistic("' . $element_id .'","' . $statistic_clean . '")' );

        wp_localize_script( 'getStatistics_osc', 'getStatistics_osc_parameters', array(
			'statistic' => $statistic_clean,
			'el_id' => $element_id,
		) );
		$span = '<span id="' . $element_id . '"></span>';
		return $span;
	}
	
	// Add shortcode for map
	add_shortcode('mappascuole', 'wp_mappascuole_shortcode');
	add_shortcode('listascuole', 'wp_listascuole_shortcode');
	// Add shortcode for complete institute list
	add_shortcode('institute_osc', 'wp_institute_osc_shortcode');
	// Add shortcode for institute
	add_shortcode('getInstitute', 'wp_getInstitute_osc_shortcode');
	// Add shortcode for complex
	add_shortcode('getComplex', 'wp_getComplex_osc_shortcode');
	// Add shortcode for Lab
	add_shortcode('getLab', 'wp_getLab_osc_shortcode');
	// Add shortcode for PC
	add_shortcode('getPC', 'wp_getPC_osc_shortcode');
	// Add shortcode for switch
	add_shortcode('getSwitch', 'wp_getSwitch_osc_shortcode');
	// Add shortcode for Donor
	add_shortcode('getDonor', 'wp_getDonor_osc_shortcode');
	// Add shortcode for lshw
	add_shortcode('getLshw', 'wp_getLshw_osc_shortcode');
	// Add shortcode for gallery creation
	add_shortcode('createGallery', 'wp_createGallery_osc_shortcode');
	// add shortcode for results
	add_shortcode('oscResults', 'wp_oscresults_shortcode');
	add_shortcode('getStatistic', 'wp_getStatistic_osc_shortcode');
