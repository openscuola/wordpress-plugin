# OpenScuola WP plugin #

## Descrizione ##

L'obiettivo principale del plugin è quello di eseguire una query al database di OpenScuola e mostrare, tramite shortcode, le informazioni sul sito web di Linux@studenti.

## Struttura ##

Il plugin contiene diversi componenti e dipendenze:

- Lo script get_data.php che esegue la "query", puoi essere chiamato con query=list o query=map a seconda dell'utilizzo, in risposta si ottengono dei dati in formato JSON
- jQuery che permette di lavorare con i dati in formato JSON
- Leaflet per poter generare la mappa dei laboratori
- Bootstrap per alcuni degli stili utilizzati
- mappa_osc.css per poter personalizzare alcuni aspetti della mappa senza intaccare il codice principale
- lista_osc.css per gestire lo stile della lista dei laboratori
- mappa_osc.js per generare la mappa dei laboratori
- lista_osc.js per generare la lista dei laboratori

## Utilizzo ##

Per poter usare il plugin è sufficiente estrarlo nella cartella dei plugin e aggiungere username, password e nome del database allo script get_data.php

Per generare la mappa dei laboratori su un post o pagina è sufficiente inserire lo shortcode [mappascuole]. Per poter ottenere la mappa solo con i laboratori che hanno un certo stato (ad esempio mostrare solo i laboratori attivi) si può inserire il parametro mostra nel seguente modo:

[mappascuola statuscode="*stati separati da virgola*"], dove stato può essere uno qualunque di quelli specificati sul DB.

Per generare la lista dei laboratori basta usare lo shortcode [listascuole]

