<?php

$config_file='config/config.ini';

$config_parsed=parse_ini_file($config_file);

$servername = $config_parsed['servername'];
$username = $config_parsed['username']; 
$password = $config_parsed['password'];
$dbname = $config_parsed['dbname'];
$charset = $config_parsed['charset'];
$wp_dir = $config_parsed['wp_dir'];

$dsn = "mysql:host=$servername;dbname=$dbname;charset=$charset";
$options = array(
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
);
try {
     $db = new PDO($dsn, $username, $password, $options);
} catch (\PDOException $e) {
     throw new \PDOException($e->getMessage(), (int)$e->getCode());
}

if($config_parsed['redis'] == "true"){
	$redis_enabled = true;
	$redis = new Redis();
	$redis->connect($config_parsed['servername'], $config_parsed['redis_port']);
	$redis->auth($config_parsed['redis_password']);
	$redis_timeout = $config_parsed['redis_timeout'];
	if ($config_parsed['rate_limiter'] == 'true'){
		$rate_limiter = true;
		$max_queries = $config_parsed['max_queries'];
		$max_interval = $config_parsed['max_interval'];
	}
}

if ($rate_limiter){
	if(!checkRate($redis, $_SERVER['REMOTE_ADDR'], $max_queries, $max_interval)){
		header('HTTP/1.1 429 Internal Server Booboo');
		header('Content-Type: application/json; charset=UTF-8');
		die(json_encode(array('message' => 'Query limit reached, please try again later', 'code' => 1)));
	}	
}


$query_get = $_GET['query'];


/* Check whether we need to query for list or map
In the future we might consider adding more querying option 
however that would obviously require working with sanitization option etc... */

switch ($query_get) {
	case "map":
		if ($redis_enabled && $redis->get($query_get)){
			$source = "Redis";
			$raw_data = unserialize($redis->get($query_get));
		}
		else{
			$source = "MySQL";
			$raw_data=queryMap($db);
			setKey($redis_enabled, $redis, $query_get, $raw_data, $redis_timeout);
		}
		break;
	case "list":
		if ($redis_enabled && $redis->get($query_get)){
			$source = "Redis";
			$raw_data = unserialize($redis->get($query_get));
		}
		else{
			$source = "MySQL";
			$raw_data=queryList($db);
			setKey($redis_enabled, $redis, $query_get, $raw_data, $redis_timeout);
		}		
		break;
	/* The two following options are used to generate the institute in a collapsible-like
	fashion, they are currently deprecated and are being gradually replaced by the get* function
	which enable the user to visualize the institute graph in a way that is more web-oriented*/
	case "institute":
		// Get institute from GET command
		$institute_get = $_GET['institute'];
		$institute_get = intval($institute_get);
		if ($institute_get > 0){
			$raw_data=queryInstitute($db, $institute_get);
		}
		else{
			header('HTTP/1.1 500 Internal Server Booboo');
			header('Content-Type: application/json; charset=UTF-8');
			die(json_encode(array('message' => 'malformed query (possibly malicious)', 'code' => 1)));
			break;
		}
		break;
	case "institute_list":
		$raw_data=instituteList($db);
		break;
	// This is the new graph-like approach
	case "getInstitute":
		$institute_get = $_GET['institute_id'];
		$institute_get = intval($institute_get);
		if ($institute_get > 0){
			$raw_data=getInstitute($db, $institute_get);
		}
		else{
			header('HTTP/1.1 500 Internal Server Booboo');
			header('Content-Type: application/json; charset=UTF-8');
			die(json_encode(array('message' => 'malformed query (possibly malicious)', 'code' => 1)));
		}
		break;
	case "getComplex":
		$complex_get = $_GET['complex_id'];
		$complex_get = intval($complex_get);
		if ($complex_get > 0){
			$raw_data=getComplex($db, $complex_get, 1);
		}
		else{
			header('HTTP/1.1 500 Internal Server Booboo');
			header('Content-Type: application/json; charset=UTF-8');
			die(json_encode(array('message' => 'malformed query (possibly malicious)', 'code' => 1)));
		}
		break;
	case "getLab":
		$lab_get = preg_replace( "/[^a-zA-Z0-9_]/", "", $_GET['lab_id']);
		$raw_data=getLab($db, $lab_get);
		break;
	case "getPC":
		$pc_get = preg_replace( "/[^a-zA-Z0-9_]/", "", $_GET['pc_id']);
		$pc_type_get = preg_replace( "/[^a-zA-Z0-9_]/", "",$_GET['pc_type']);
		$raw_data=getPC($db, $pc_get, $pc_type_get);	
		break;
	case "getSwitch":
		$switch_get = preg_replace( "/[^a-zA-Z0-9_-]/", "", $_GET['switch_name']);
		$raw_data=getSwitch($db, $switch_get);	
		break;
	case "getLshw":
		$pc_get = preg_replace("/[^a-zA-Z0-9_]/", "", $_GET['pc_id']);
		$pc_type_get = preg_replace( "/[^a-zA-Z0-9_]/", "",$_GET['pc_type']);
		$raw_data=getLshw($db, $pc_type_get, $pc_get);
		break;
	case "getDonor":
		$donor_get = preg_replace( "/[^a-zA-Z0-9_]/", "",$_GET['donor_id']);
		$raw_data=getDonor($db, $donor_get);
		break; 
	case "getLabImages":
		$lab_get = preg_replace( "/[^0-9_]/", "", $_GET['lab_id']);
		$raw_data=getLabImages($lab_get);
		break;
	case "getImages":
		// accept only data containing alphabetical characters	
		$directory_get= preg_replace( "/[^a-zA-Z0-9_]/", "", $_GET['gallery_id']);
		$raw_data = getImages($directory_get);
		break;
	case "getInstalled":
		if ($redis_enabled && $redis->get($query_get)){
			$source = "Redis";
			$raw_data = unserialize($redis->get($query_get));
		}
		else{
			$source = "MySQL";
			$raw_data = getInstalled($db);
			setKey($redis_enabled, $redis, $query_get, $raw_data, $redis_timeout);
		}		
		break;
	case "getInstalledLabs":
		if ($redis_enabled && $redis->get($query_get)){
			$source = "Redis";
			$raw_data = unserialize($redis->get($query_get));
		}
		else{
			$source = "MySQL";
			$raw_data = getInstalledLabs($db);
			setKey($redis_enabled, $redis, $query_get, $raw_data, $redis_timeout);
		}		
		break;
	case "getRequestedLabs":
		if ($redis_enabled && $redis->get($query_get)){
			$source = "Redis";
			$raw_data = unserialize($redis->get($query_get));
		}
		else{
			$source = "MySQL";
			$raw_data = getRequestedLabs($db);
			setKey($redis_enabled, $redis, $query_get, $raw_data, $redis_timeout);
		}		
		break;
	// Default behaviour is to return error
	default:
		header('HTTP/1.1 500 Internal Server Booboo');
        header('Content-Type: application/json; charset=UTF-8');
        die(json_encode(array('message' => 'Invalid query', 'code' => 1)));
}
// Send response if option is recognized
header('Content-Type: application/json');
$encoded_data=utf8_converter($raw_data);
if ($_GET['debug'] == 'true'){
	print $source;
}
print json_encode($encoded_data);
// Destroy db variable
$db = NULL;

/* Functions that get data from database */

// Functions which generate the content graph (getInstitute, getComplex, getLab, getPC)
function getInstitute($db, $institute_id){
	
	// Get institute information
	$institute_query = $db->prepare("SELECT name, type, address, tel, url, mail, notes, lat, lon FROM institutes WHERE id=?;");
	if (!$institute_query->execute([$institute_id])) {
		exit("An SQL error occured.\n");
	}
	// Insert institute name and type into variable
	$r_institute=$institute_query->fetch(PDO::FETCH_ASSOC);
	// Fetch institute type string once institute has been initialized and substitute
	$institute_type_query = $db->prepare("SELECT name FROM institute_types WHERE type=?");
	if (!$institute_type_query->execute([$r_institute['type']])) {
		exit("An SQL error occured.\n");
	}
	$r_institute_type=$institute_type_query->fetch(PDO::FETCH_ASSOC);
	// Assign proper name and type
	$rows=$r_institute;
	$rows['type']=$r_institute_type['name'];

	// Query complexes
	$complexes_query =$db->prepare("SELECT id, name, tel, address, url, type FROM schoolcomplexes WHERE institute=?;");
	if (!$complexes_query->execute([$institute_id])) {
		exit("An SQL error occured.\n");
	}

	// Loop through the school complexes 
	while($r = $complexes_query->fetch(PDO::FETCH_ASSOC)) {
		// Query DB for PCs and Labs and in the found school complexes
		$type_query = $db->prepare("SELECT name FROM school_types WHERE type=?;");
		if (!$type_query->execute([$r['type']])) {
			exit("An SQL error has occured.\n");
		}
		// Set type to actual string
		$type=$type_query->fetch(PDO::FETCH_ASSOC);
		$r['type_text']=$type['name'];
		// Adds new row with new row of query;
		$rows['complexes'][] = $r;
	}	
	return $rows;
}

function getComplex($db, $complex_id, $getpc){
	// Prepare school complex query
	$complexes_query = $db->prepare("SELECT id, type, name, address, tel, institute, url, notes, lat, lon FROM schoolcomplexes WHERE id=?");
	
	if (!$complexes_query->execute([$complex_id])) {
		exit("An SQL error occured.\n");
	}
	// Insert complex name and type into variable
	$rows=$complexes_query->fetch(PDO::FETCH_ASSOC);
	// Fetch complex type string once institute has been initialized and substitute
	$complex_type_query = $db->prepare("SELECT name FROM school_types WHERE type=?;");
	if (!$complex_type_query->execute([$rows['type']])) {
		exit("An SQL error occured.\n");
	}
	$r_complex_type=$complex_type_query->fetch(PDO::FETCH_ASSOC);
	// Assign proper type
	$rows['type']=$r_complex_type['name'];
	// Loop through the laboratories 
	$lab_query = $db->prepare("SELECT name, code, statuscode FROM laboratories WHERE schoolcomplex=?");
	if (!$lab_query->execute([$rows['id']])) {
		exit("An SQL error occured.\n");
	}

	if ($getpc == 1) {
		// Add lab to lab array and fetch PCs data
		while ($r_lab = $lab_query->fetch(PDO::FETCH_ASSOC)){
			$tmp_lab = $r_lab;
			$lab_status_query = $db->prepare("SELECT name FROM lab_status WHERE code=?");
			if (!$lab_status_query->execute([$r_lab['statuscode']])) {
				exit("An SQL error has occured.\n");
			}
			$r_status= $lab_status_query->fetch(PDO::FETCH_ASSOC);
			// Set status code
			// Assign text to status code to show in popup
			$tmp_lab['lab_status_text']=$r_status['name'];
			$rows['labs'][]=$tmp_lab;
		}
	}
	
	// Query to get institute name so that in the web interface we can use it to go back in the graph
	$institute_query = $db->prepare("SELECT name, type FROM institutes WHERE id=?");
	if (!$institute_query->execute([$rows['institute']])) {
		exit("An SQL error occured.\n");
	}
	$institute_reply=$institute_query->fetch(PDO::FETCH_ASSOC);
	$rows['institute_text']['name']=$institute_reply['name'];
	// Fetch the type
	$institute_type_query = $db->prepare("SELECT name FROM institute_types WHERE type=?");
	if (!$institute_type_query->execute([$institute_reply['type']])) {
		exit("An SQL error occured.\n");
	}
	$institute_type_rs=$institute_type_query->fetch(PDO::FETCH_ASSOC);
	$rows['institute_text']['type']=$institute_type_rs['name'];
	return $rows;
}


function getLab($db, $lab_id){
	$lab_query = $db->prepare("SELECT name, code, statuscode, delivery, schoolcomplex, INVALSI, OSupdate, OS, SecondaryOS, Bandwidth, notes, client FROM laboratories WHERE code=?;");
	if (!$lab_query->execute([$lab_id])) {
		exit("An SQL error occured.\n");
	} 
	
	// Fetch PCs data
	$r_lab = $lab_query->fetch(PDO::FETCH_ASSOC);
	$tmp_lab = $r_lab;
	$lab_status_query = $db->prepare("SELECT name FROM lab_status WHERE code=?;");
	if (!$lab_status_query->execute([$r_lab['statuscode']])) {
		exit("An SQL error occured.\n");
	}
	$r_status= $lab_status_query->fetch(PDO::FETCH_ASSOC);
	// Set status code
	$tmp_lab['lab_status']=$r_lab['statuscode'];
	// Fetch PCs
	$pc_query= $db->prepare("SELECT pctype, pcid, ip, user, offspring FROM pc_laboratories WHERE labcode=?;");
	if (!$pc_query->execute([$r_lab['code']])) {
		exit("An SQL error occured.\n");
	}
	// Create array with list of pcs along with their data
	while ($r_pc = $pc_query->fetch(PDO::FETCH_ASSOC)){
		// Get additional PC data from the pc table
		$pc_query2=$db->prepare("SELECT mac FROM pc WHERE type=? AND id=?;");
		if (!$pc_query2->execute([$r_pc['pctype'], $r_pc['pcid']])) {
			exit("An SQL error has occured.\n");
		}
		$r_pc2=$pc_query2->fetch(PDO::FETCH_ASSOC);
		$r_pc['mac']=$r_pc2['mac'];
		$tmp_lab['lab_pcs'][]=$r_pc;
	}

	// Get configuration files from lab_files table
	// REMEMBER BACKTICKS (`) ON ORDER, it is a reserved mysql keyword 
	$lab_files_query=$db->prepare("SELECT name, file, `order` FROM lab_files WHERE labcode=? ORDER BY `order`;");
	if (!$lab_files_query->execute([$lab_id])) {
		exit("An SQL error occured.\n");

	}
	while ($r_lab_files = $lab_files_query->fetch(PDO::FETCH_ASSOC)){
		$tmp_lab['lab_files'][]=$r_lab_files;
	}

	// Get switches and append them to the lab_switches list

	$lab_switches_query=$db->prepare("SELECT ip, name, model FROM switchs WHERE labcode=?");
	if (!$lab_switches_query->execute([$lab_id])) {
		exit("An SQL error occured.\n");
	}
	while ($r_lab_switches = $lab_switches_query->fetch(PDO::FETCH_ASSOC)){
		$tmp_lab['lab_switches'][]=$r_lab_switches;
	}
	
	// Assign text to status code to show in popup
	$tmp_lab['lab_status_text']=$r_status['name'];
	$rows=$tmp_lab;	
	$complex_query = $db->prepare("SELECT name, type, id FROM schoolcomplexes WHERE id=?;");
	if (!$complex_query->execute([$rows['schoolcomplex']])) {
		exit("An SQL error occured.\n");
	}
	// Query complex type
	$complex_r=$complex_query->fetch(PDO::FETCH_ASSOC);
	$complex_type_query = $db->prepare("SELECT name FROM school_types WHERE type=?;");
	if (!$complex_type_query->execute([$complex_r['type']])) {
		exit("An SQL error occured.\n");
	}
	$r_complex_type=$complex_type_query->fetch(PDO::FETCH_ASSOC);
	// Assign proper type
	$complex_r['type']=$r_complex_type['name'];
	$rows['complex']=$complex_r;
	
	return $rows;
}

function getPC($db, $pc_id, $pc_type){
	// Get specific PC data from the pc table (ie. mac address, model, lshw...)
	$pc_query=$db->prepare("SELECT sn, pn, model, donor, mac, mac_iface_out, lshw, notes FROM pc WHERE type=? AND id=?;");
	if (!$pc_query->execute([$pc_type, $pc_id])) {
		exit("An SQL error occured.\n");
	}
	$pc_rs=$pc_query->fetch(PDO::FETCH_ASSOC);
	
	// Query donor info from donor id and replace it in final response
	$donor_query=$db->prepare("SELECT name, code FROM pc_donor WHERE code=?;");
	if (!$donor_query->execute([$pc_rs['donor']])) {
		exit("An SQL error occured.\n");
	}
	$pc_rs['donor']= $donor_query->fetch(PDO::FETCH_ASSOC);
	
	$rows['pc_info']=$pc_rs;
	// Fetch PCs
	$pc_laboratories_query=$db->prepare('SELECT labcode, ip, user, hostname, offspring, notes, activationdate, deliverydate FROM pc_laboratories WHERE pctype=? AND pcid=?;');
	if (!$pc_laboratories_query->execute([$pc_type, $pc_id])) {
		exit("An SQL error occured.\n");
	}
	// Get PC data
	$pc_laboratories_rs = $pc_laboratories_query->fetch(PDO::FETCH_ASSOC);
	// Insert this array inside the response
	$rows['position']=$pc_laboratories_rs;
	// Get the name of the laboratory
	if ($rows['position']['labcode'] != NULL){
		$lab_query='SELECT name FROM laboratories WHERE code=' . $rows['position']['labcode'] . ';';
		$lab_rs= $db->query($lab_query);
		if (!$lab_rs){
			exit("An SQL error occured.\n");
		}
		$lab_r=$lab_rs->fetch(PDO::FETCH_ASSOC);
		$rows['position']['name']=$lab_r['name'];
	}
	return $rows;
}

function getSwitch($db, $switch_name){
	// Get specific PC data from the pc table (ie. mac address, model, lshw...)
	$switch_query='SELECT labcode, name, ip, model, notes FROM switchs WHERE name="' . $switch_name . '";';
	$switch_rs = $db->query($switch_query);
	if (!$switch_rs) {
		exit("An SQL error occured.\n");
	}
	$switch_response= $switch_rs->fetch(PDO::FETCH_ASSOC);

	// Lab query 
	$lab_query='SELECT name, code FROM laboratories WHERE code="' . $switch_response['labcode'] . '";';
	$lab_query_rs = $db->query($lab_query);
	if (!$lab_query_rs) {
		exit("An SQL error occured.\n");
	}

	$switch_response['lab']= $lab_query_rs->fetch(PDO::FETCH_ASSOC);


	return $switch_response;
}

function getDonor($db, $donor_id){
	// Get Donor Name based on ID
	$donor_query=$db->prepare("SELECT name, code FROM pc_donor WHERE code=?;");
	if (!$donor_query->execute([$donor_id])){
		exit("An SQL error occured.\n");
	}
	$donor_rs= $donor_query->fetch(PDO::FETCH_ASSOC);

	// Obtain all PCs from the donor
	$pc_donated_query=$db->prepare("SELECT type, id FROM pc WHERE donor=?;");
	if (!$pc_donated_query->execute([$donor_id])){
		exit("An SQL error occured.\n");
	}

	while ($pc_donated_r = $pc_donated_query->fetch(PDO::FETCH_ASSOC)){
		// Fetch associated LAB data
		$labcode_donated_query= $db->prepare("SELECT labcode FROM pc_laboratories WHERE pctype=? AND pcid=?;");
		if (!$labcode_donated_query->execute([$pc_donated_r['type'], $pc_donated_r['id']])){
			exit("An SQL error occured.\n");
		}	
		$labcode_donated_r=$labcode_donated_query->fetch(PDO::FETCH_ASSOC);
		if ( $labcode_donated_r['labcode'] > 0 ) {
			$lab_donated_info_query=$db->prepare("SELECT schoolcomplex FROM laboratories WHERE code=?;");
			if (!$lab_donated_info_query->execute([$labcode_donated_r['labcode']])){
				exit("An SQL error occured.\n");
			}
			$lab_donated_info_r=$lab_donated_info_query->fetch(PDO::FETCH_ASSOC);
			$pc_tmp['lab']=$lab_donated_info_r;
			if ($lab_donated_info_r['schoolcomplex'] > 0){
				$complex_donated_r=getComplex($db, $lab_donated_info_r['schoolcomplex'], 0);
				$pc_tmp['complex']=$complex_donated_r;
			}
		}
		else {
			$pc_tmp['complex']="";
		}
		$pc_tmp['pc']=$pc_donated_r;
		$donor['pc'][]=$pc_tmp;
	}
	
	$donor['name']=$donor_rs['name'];
	$donor['code']=$donor_rs['code'];	
	return $donor;
}	

function getLshw($db, $pc_type_get, $pc_get){
	// Fetch PCs
	$pc_query=$db->prepare("SELECT lshw FROM pc WHERE type=? AND id=?;");
	if (!$pc_query->execute([$pc_type_get, $pc_get])) {
		exit("An SQL error occured.\n");
	}
	$pc_rs=$pc_query->fetch(PDO::FETCH_ASSOC);
	$lab_query= $db->prepare("SELECT labcode FROM pc_laboratories WHERE pctype=? AND pcid=?");
	if (!$lab_query->execute([$pc_type_get, $pc_get])) {
		exit("An SQL error occured.\n");
	}
	$lab_rs=$lab_query->fetch(PDO::FETCH_ASSOC); 
	$rows['lshw']=$pc_rs['lshw'];
	$rows['labcode']=$lab_rs['labcode'];
	return $rows;
}

function getLabImages($lab_id){
	// Fetch directory list
	global $wp_dir;
	$dir = $wp_dir . '/wp-content/uploads/galleries/lab/' . $lab_id;
	$files = scandir($dir, 0);
	
	// Use array_values otherwise the indexing will start from 2 which will cause issues when passing to JS
	$files = array_values(array_diff($files, array('.', '..')));
	
	return $files;
}

function getImages($directory){
	// Get images inside a specific directory in uploads
	global $wp_dir;
	$dir = $wp_dir . '/wp-content/uploads/galleries/' . $directory;
	$files = scandir($dir, 0);
	
	$files = array_values(array_diff($files, array('.', '..')));
	
	return $files;

}

// Get the list of complexes
function queryList($db){
	$sql = "SELECT id, name, tel, address, url, type, institute FROM schoolcomplexes;";
	$rs = $db->query($sql);
	if (!$rs) {
		echo "An SQL error occured.\n";
		exit;
	}
	$rows = array();
	while($r = $rs->fetch(PDO::FETCH_ASSOC)) {
		// Query DB for labs and institutes in selected school complex
		$lab_query = 'SELECT name, statuscode FROM laboratories WHERE schoolcomplex=' . $r['id'] . ';';
		$type_query = 'SELECT name FROM school_types WHERE type="' . $r['type'] . '";';
		$institute_query = 'SELECT name, type FROM institutes WHERE id="' . $r['institute'] . '";';
		$lab_rs = $db->query($lab_query);
		$institute_rs = $db->query($institute_query);
		if (!$lab_rs) {
			exit("An SQL error has occured.\n");
		}
		$type_rs = $db->query($type_query);
		if (!$type_rs) {
			exit("An SQL error has occured.\n");
		}
		// Add lab to lab array
		while ($r_lab = $lab_rs->fetch(PDO::FETCH_ASSOC)){
			$r['labs'][]= $r_lab['name'];
			$lab_status_query = 'SELECT name FROM lab_status WHERE code="' . $r_lab['statuscode'] . '";';
			$status_rs = $db->query($lab_status_query);
			if (!$status_rs) {
				exit("An SQL error has occured.\n");
			}
			$r_status= $status_rs->fetch(PDO::FETCH_ASSOC);
			// Set status code
			$r['lab_status'][]=$r_lab['statuscode'];
			// Assign text to status code to show in popup
			$r['lab_status_text'][]=$r_status['name'];
		}
		// Set type to actual string
		$type=$type_rs->fetch(PDO::FETCH_ASSOC);
		$r['type_text']=$type['name'];
		// Add institute name and type
		$r['institute']=$institute_rs->fetch(PDO::FETCH_ASSOC);
		// Fetch institute type string once institute has been initialized and substitute
		$institute_type_query = 'SELECT name FROM institute_types WHERE type="' . $r['institute']['type'] . '";';
		$institute_type_rs = $db->query($institute_type_query);
		if (!$institute_type_rs) {
			exit("An SQL error has occured.\n");
		}
		$r['institute']['type']=$institute_type_rs->fetch(PDO::FETCH_ASSOC);
		$r['institute']['type']=$r['institute']['type']['name'];
		// Adds new row with new row of query;
		$rows[] = $r;
	}	
	return $rows;

}

// Query data to generate map of complexes
function queryMap($db){
	$sql = "SELECT id, name, type, tel, address, url, lat, lon, institute FROM schoolcomplexes;";
	
	$rs = $db->query($sql);
	
	if (!$rs) {
		echo "An SQL error occured.\n";
		exit;
	}
	$rows = array();
	while($r = $rs->fetch(PDO::FETCH_ASSOC)) {
		// Query DB for labs in selected school complex
		$lab_query = 'SELECT name, code, statuscode FROM laboratories WHERE schoolcomplex=' . $r['id'] . ' AND (statuscode="R" OR statuscode="RA");';
		$type_query = 'SELECT name FROM school_types WHERE type="' . $r['type'] . '";';
		$institute_query = 'SELECT name, type, id FROM institutes WHERE id="' . $r['institute'] . '";';		
		$lab_rs = $db->query($lab_query);		
		if (!$lab_rs) {
			exit("An SQL error has occured.\n");
		}
		$type_rs = $db->query($type_query);
		if (!$type_rs) {
			exit("An SQL error has occured.\n");
		}
		// Get institute data
		$institute_rs = $db->query($institute_query);
		if (!$institute_rs) {
			exit("An SQL error has occured.\n");
		}
		$r['institute']=$institute_rs->fetch(PDO::FETCH_ASSOC);
		// Fetch institute type string once institute has been initialized and substitute
		$institute_type_query = 'SELECT name FROM institute_types WHERE type="' . $r['institute']['type'] . '";';
		$institute_type_rs = $db->query($institute_type_query);
		if (!$institute_type_rs) {
			exit("An SQL error has occured.\n");
		}
		$r['institute']['type']=$institute_type_rs->fetch(PDO::FETCH_ASSOC);
		$r['institute']['type']=$r['institute']['type']['name'];
		// Add lab to lab array
		while ($r_lab = $lab_rs->fetch(PDO::FETCH_ASSOC)){
			$r['labs'][]= $r_lab['name'];
			/*$lab_status_query = 'SELECT name FROM lab_status WHERE code="' . $r_lab['statuscode'] . '";';
			$status_rs = $db->query($lab_status_query);
			if (!$status_rs) {
				exit("An SQL error has occured.\n");
			}
			$r_status= $status_rs->fetch(PDO::FETCH_ASSOC);*/
			// Set status code
			$r['lab_status'][]=$r_lab['statuscode'];
			$r['lab_code'][]=$r_lab['code'];
			// Assign text to status code to show in popup
			$r['lab_status_text'][]=$r_status['name'];
			
		}
		// Set type to actual string
		$type=$type_rs->fetch(PDO::FETCH_ASSOC);
		$r['type_text']=$type['name'];
		// Adds new row with new row of query;
		$rows[] = $r;
	}	
	return $rows;
}

// Useful function that converts data to UTF-8 to fix problems with various latin characters
function utf8_converter($array)
{
    array_walk_recursive($array, function(&$item, $key){
                $item = utf8_encode($item);
    });
 
    return $array;
}

// Query data to be inserted in list of labs
// Old mode, see comment in switch statement!
function queryInstitute($db, $institute_id){
	// Prepare school complex query
	$complexes_query = "SELECT id, name, tel, address, url, type, institute FROM schoolcomplexes WHERE institute=" . $institute_id . ';';
	$rs = $db->query($complexes_query);
	// Get institute information
	$institute_query = 'SELECT name, type FROM institutes WHERE id="' . $institute_id . '";';
	$institute_rs = $db->query($institute_query);
	if (!$institute_rs) {
		exit("An SQL error occured.\n");
	}
	// Insert institute name and type into variable
	$r_institute=$institute_rs->fetch(PDO::FETCH_ASSOC);
	// Fetch institute type string once institute has been initialized and substitute
	$institute_type_query = 'SELECT name FROM institute_types WHERE type="' . $r_institute['type'] . '";';
	$institute_type_rs = $db->query($institute_type_query);
	if (!$institute_type_rs) {
		exit("An SQL error occured.\n");
	}
	$r_institute_type=$institute_type_rs->fetch(PDO::FETCH_ASSOC);
	// Assign proper name and type
	$rows['name']=$r_institute['name'];
	$rows['type']=$r_institute_type['name'];
	// Loop through the school complexes 
	while($r = $rs->fetch(PDO::FETCH_ASSOC)) {
		// Query DB for PCs and Labs and in the found school complexes
		$lab_query = 'SELECT name, code, statuscode, delivery, INVALSI, OSupdate, OS, SecondaryOS, notes, client FROM laboratories WHERE schoolcomplex=' . $r['id'] . ';';
		$type_query = 'SELECT name FROM school_types WHERE type="' . $r['type'] . '";';
		$lab_rs = $db->query($lab_query);
		if (!$lab_rs) {
			exit("An SQL error has occured.\n");
		}
		$type_rs = $db->query($type_query);
		if (!$type_rs) {
			exit("An SQL error has occured.\n");
		}
		// Add lab to lab array and fetch PCs data
		while ($r_lab = $lab_rs->fetch(PDO::FETCH_ASSOC)){
			$tmp_lab = $r_lab;
			$lab_status_query = 'SELECT name FROM lab_status WHERE code="' . $r_lab['statuscode'] . '";';
			$status_rs = $db->query($lab_status_query);
			if (!$status_rs) {
				exit("An SQL error has occured.\n");
			}
			$r_status= $status_rs->fetch(PDO::FETCH_ASSOC);
			// Set status code
			$tmp_lab['lab_status']=$r_lab['statuscode'];
			// Assign text to status code to show in popup
			$tmp_lab['lab_status_text']=$r_status['name'];
			// Fetch PCs
			$pc_query='SELECT pctype, pcid, ip, notes, activationdate, deliverydate FROM pc_laboratories WHERE labcode=' . $r_lab['code'] . ';';
			$pc_rs = $db->query($pc_query);
			if (!$pc_rs) {
				exit("An SQL error has occured.\n");
			}
			// Create array with list of pcs along with their data
			while ($r_pc = $pc_rs->fetch(PDO::FETCH_ASSOC)){
				$lab_pcs_tmp[]=$r_pc;
			}
			// Insert this array inside the response
			$tmp_lab['lab_pcs'] =$lab_pcs_tmp;
			$r['labs'][]=$tmp_lab;
			unset($lab_pcs_tmp);
		}
		// Set type to actual string
		$type=$type_rs->fetch(PDO::FETCH_ASSOC);
		$r['type_text']=$type['name'];
		// Adds new row with new row of query;
		$rows['complexes'][] = $r;
	}	
	return $rows;
	
}
// Query list of institutes (temporary function used to implement the list)
function instituteList($db){
	$institute_query = 'SELECT id FROM institutes;';
	$rs = $db->query($institute_query);
	if (!$rs) {
		echo "An SQL error occured.\n";
		exit;
	}
	$rows = array();
	while($r = $rs->fetch(PDO::FETCH_ASSOC)) {
		$rows[]=$r;
	}
	return $rows;
}

function getInstalled($db){
	$installed_query= $db->prepare("SELECT COUNT(*) FROM laboratories INNER JOIN pc_laboratories ON laboratories.code = pc_laboratories.labcode WHERE laboratories.statuscode != 'MC';");
	if (!$installed_query->execute()) {
		exit("An SQL error occured.\n");
	}
	$r = $installed_query->fetch(PDO::FETCH_ASSOC);
	$response['response'] = $r['COUNT(*)'];
	return $response;
}

function getInstalledLabs($db){
	$installed_query= $db->prepare("SELECT COUNT(*) FROM laboratories WHERE laboratories.statuscode != 'MC';");
	if (!$installed_query->execute()) {
		exit("An SQL error occured.\n");
	}
	$r = $installed_query->fetch(PDO::FETCH_ASSOC);
	$response['response'] = $r['COUNT(*)'];
	return $response;
}

function getRequestedLabs($db){
	$installed_query= $db->prepare("SELECT id FROM schoolcomplexes LEFT OUTER JOIN laboratories ON schoolcomplexes.id = laboratories.schoolcomplex WHERE laboratories.schoolcomplex IS NULL;");
	if (!$installed_query->execute()) {
		exit("An SQL error occured.\n");
	}
	$count = 0;
	while ($installed_query->fetch(PDO::FETCH_ASSOC)){
		$count = $count + 1;
	}
	$response['response'] = $count;
	return $response;
}

// Rate limiter

function checkRate($redis, $remote_addr, $max_queries, $max_interval){
	if ($redis->get($remote_addr)){
		$num_access = $redis->get($remote_addr);
		if ($num_access >= $max_queries){
			print $num_access;
			return false;
		}
		else{
			$redis->incr($remote_addr);
			return true;
		}
		
	}
	else{
		$num_access = 1;
		$redis->set($remote_addr, $num_access);
		$redis->expire($remote_addr, $max_interval);
		return true;
	}
}

function setKey($redis_enabled, $redis, $key, $value, $timeout){
	if ($redis_enabled){
		$redis->set($key, serialize($value));
    	$redis->expire($key, $timeout);
	}
}
