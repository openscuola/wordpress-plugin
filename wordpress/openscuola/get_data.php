<?php

/* This file allows to obtain raw data directly from the database
this is not used by the frontend Wordpress plugin but by our internal
tools */

$config_file='config/config.ini';

$config_parsed=parse_ini_file($config_file);

$servername = $config_parsed['servername'];
$username = $config_parsed['username']; 
$password = $config_parsed['password'];
$dbname = $config_parsed['dbname'];
$charset = $config_parsed['charset'];

$dsn = "mysql:host=$servername;dbname=$dbname;charset=$charset";
$options = array(
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
);
try {
     $db = new PDO($dsn, $username, $password, $options);
} catch (\PDOException $e) {
     throw new \PDOException($e->getMessage(), (int)$e->getCode());
}

$query_get = $_GET['query'];

switch ($query_get) {
	case "getMacs":
		$raw_data=getMacs($db);
		break;
	case "getMac":
		$pcid=$_GET['pcid'];
		$pctype=$_GET['pctype'];
		$raw_data=getMac($db, $pcid, $pctype);
		break;
	case "getLab":
		$labcode=$_GET['labcode'];
		$raw_data=getLab($db, $labcode);
		break;
	// Default behaviour is to return error
	default:
		header('HTTP/1.1 500 Internal Server Booboo');
        header('Content-Type: application/json; charset=UTF-8');
        die(json_encode(array('message' => 'Invalid query', 'code' => 1)));
		break;
}
// Send response if option is recognized
header('Content-Type: application/text');
$encoded_data=utf8_converter($raw_data);
print $encoded_data;
// Destroy db variable
$db = NULL;

/* Functions that get data from database */

function getLab($db, $labcode){
	$pc_query="SELECT pctype, pcid FROM pc_laboratories WHERE labcode=". $labcode . ";";
	$rs = $db->query($pc_query);
	while($r = $rs->fetch(PDO::FETCH_ASSOC)){
		$response .= getMacPC($db, $r['pcid'], $r['pctype']) . "\n";	
	}
	return $response;
}

function getMac($db, $pc_id, $pc_type){
	// Prepare school complex query
	$mac_query = "SELECT mac FROM pc WHERE id=" . $pc_id . ' AND type="'. $pc_type . '";';
	$rs = $db->query($mac_query);
	$r = $rs->fetch(PDO::FETCH_ASSOC);
	// Add leading zeros to PC id
	$pc_id=strval($pc_id);
	$pc_id=sprintf('%03d', $pc_id);
	return $pc_type . $pc_id . " " . $r['mac'];
}

function getMacs($db){
	
	// Prepare school complex query
	$mac_query = 'SELECT id, type, mac FROM pc;';
	$rs = $db->query($mac_query);

	while($r = $rs->fetch(PDO::FETCH_ASSOC)){
		// Add leading zeros to PC id
		$r['id']=strval($r['id']);
		$r['id']=sprintf('%03d', $r['id']);                        
		$response .= $r['type'] . $r['id'] . " " . $r['mac'] . "\n"; 	
	}
	return $response;
}

// Useful function that converts data to UTF-8 to fix problems with various latin characters
function utf8_converter($array)
{
    array_walk_recursive($array, function(&$item, $key){
                $item = utf8_encode($item);
    });
 
    return $array;
}
