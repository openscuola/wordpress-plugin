/* Get the div we are working on and assign it to a variable
we will only modify this element from now on and add whatever we want to it */

jQuery(document).ready(function(){
	// Useful HTML elements;
	
	var gallery_div = document.getElementById('osc_gallery');
	gallery_div.setAttribute('class', 'gallery');
	gallery_div.setAttribute('itemscope', '');	
	gallery_div.setAttribute('itemtype', 'http://schema.org/ImageGallery');
	
	images_div =document.createElement("div");
	images_div.setAttribute('class', 'images');
	
	thumbs_div =document.createElement("div");
	thumbs_div.setAttribute('class', 'thumbs');
	
	// setup regex to only get one resolution
	res_patt=/-\d{2,5}x\d{2,5}\..*/i;
	thumb_patt=/.*-1024*x[0-9]*.*/i;
	first=1;
	
	// Get list of thumbs from PHP and create gallery
	
	jQuery.getJSON(getImages_osc_vars.base_dir + 'get_json_data.php', {query: "getImages", gallery_id: getImages_osc_parameters.gallery_id}, function (data) {

		// create laboratory images
		for (var i=0; i<data.length; i++){
			if (!res_patt.test(data[i])){
				image_div=document.createElement("div");
				image_div.style.backgroundImage="url(" + getImages_osc_vars.base_dir+ "../../uploads/galleries/" + getImages_osc_parameters.gallery_id + "/" + data[i] + ")";
				images_div.appendChild(image_div);
				if (first){
					image_div.setAttribute('class', 'active');
					first=0;
				}
			}
			else if (thumb_patt.test(data[i])){
				thumb_div=document.createElement("div");
				thumb_div.style.backgroundImage="url(" + getImages_osc_vars.base_dir+ "../../uploads/galleries/" + getImages_osc_parameters.gallery_id + "/" + data[i] + ")";
				thumbs_div.appendChild(thumb_div);
			}
		}
		
		// Add span elements for arrows
		span_left=document.createElement("span");
		span_left.setAttribute('class', 'left');
		span_right=document.createElement("span");
		span_right.setAttribute('class', 'right');
		
		images_div.appendChild(span_left);
		images_div.appendChild(span_right);	
		
		gallery_div.appendChild(images_div);
		gallery_div.appendChild(thumbs_div);
		galleryInit(gallery_div);
	});
});
