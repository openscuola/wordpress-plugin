/* Get the div we are working on and assign it to a variable
we will only modify this element from now on and add whatever we want to it */
jQuery(document).ready(function(){
	// Useful HTML elements;
	
	var istituto=document.getElementById('osc_institute');
	var curr_complex;
	
	// Loop through institutes 
	jQuery.getJSON(institute_osc_vars.base_dir + 'get_json_data.php', {query: "institute_list", institute: "0"}, function (data) {
		for (var i = 0; i < data.length ; i++){
			getInstitute(data[i].id);
		}
	});
	
	
	
function getInstitute(inst_id_query){
	// AJAX request to get the list of complexes, in the query you find "institute" which is the type of query we are doing and the institute variable which indicates the institude id we want
	// to get the information of
	jQuery.getJSON(institute_osc_vars.base_dir + 'get_json_data.php', {query: "institute", institute: inst_id_query}, function (data) {
		// As in the previous script here we work with the data
		// Remember that once you are outside of this function the received data won't be accessible anymore due to the async nature of AJAX
		var title=document.createElement("h3");
		// Create title with institute name
		title.setAttribute('class', 'listascuole');
		title.innerHTML = data.type + " "+ data.name + "<br>";
		istituto.append(title);
		for (var i=0; i<data.complexes.length; i++){
				if (data.complexes[i].labs != null){
				var labs_count = "<installed>Laboratori installati: "  + data.complexes[i].labs.length+ "</installed> ";
				var labs_list=new String("<b>Laboratori OpenScuola: </b><br>");
				for (var j = 0; j<data.complexes[i].labs.length; j++){
					labs_list=labs_list.concat("<b>" + data.complexes[i].labs[j].name + "</b>: " + data.complexes[i].labs[j].lab_status_text);
					if (j != data.complexes[i].labs.length - 1){
						labs_list=labs_list.concat("<br>");
					}
				}
				// Append content to collapsible
				var curr_lab=document.createElement("div");
				curr_lab.setAttribute('class', 'content');
				curr_lab.innerHTML=labs_list;
				}
				// If there are no laboratories then show this message
				else {
					var labs_count = "<requested>La scuola ha richiesto l'installazione dei laboratori</requested>";
					labs_list="";
				}
				
				genComplexCollapsible(data.complexes[i].name, data.complexes[i].type_text, data.complexes[i].address, data.complexes[i].url, data.complexes[i].tel, labs_count, data.complexes[i].labs, istituto);
		
		}
		
	});
	
	function genComplexCollapsible(name, type, address, url, tel, labs_count, labs, main_div){
		// Create collapsible
		var curr_complex=document.createElement("div");
		curr_complex.setAttribute('class', 'collapsible');
		var curr_complex_content=document.createElement("div");
		curr_complex_content.setAttribute('class', 'content');
		// Set the collapsible text and append
		curr_complex.innerHTML= type + " " + name + " " + labs_count;
		main_div.appendChild(curr_complex);
		var curr_complex_content_text=document.createElement("div");
		curr_complex_content_text.innerHTML= "<b>Indirizzo: </b>"+ address + "<br> <b>Sito web:</b> <a href='" + url + "'>" + url + "</a><br><b> Telefono:</b>" + tel + "<br> <b> Lista laboratori:</b></br>";
		curr_complex_content.appendChild(curr_complex_content_text);
		
		// Generate lab collapsible
		if (labs != null){
			for (var i = 0; i<labs.length; i++){
				var curr_lab=document.createElement("div");
				curr_lab.setAttribute('class', 'collapsible');
				var curr_lab_content=document.createElement("div");
				curr_lab_content.setAttribute('class', 'content');
				var curr_lab_content_text=document.createElement("div");
				curr_lab.innerHTML=labs[i].name + " <b>" + labs[i].lab_status_text + "</b> " + "<b>Codice: </b>" + labs[i].code;
				curr_lab_content_text.innerHTML=  "<b>Consegna: </b>" + labs[i].delivery + "<br><b>Data adeguamento INVALSI: </b>" + labs[i].INVALSI + "<br><b>Aggiornamento OS: </b>" + labs[i].OSupdate + "<br><b>OS: </b>" + labs[i].OS + "<br><b>OS secondario: </b>" + labs[i].SecondaryOS + "<br><b>Notes: </b>" + labs[i].notes + "<br><b>Client: </b>" + labs[i].client;
				curr_lab_content.appendChild(curr_lab_content_text);
				for (var j = 0; j < labs[i].lab_pcs.length; j++){
					var curr_pc = document.createElement("div");
					curr_pc.setAttribute('class', 'collapsible');
					curr_pc.innerHTML="Postazione " + labs[i].lab_pcs[j].pctype + labs[i].lab_pcs[j].pcid;
					var curr_pc_content=document.createElement("div");
					var curr_pc_content_text=document.createElement("div");
					curr_pc_content.setAttribute('class', 'content');
					curr_pc_content_text.innerHTML= "IP: " + labs[i].lab_pcs[j].ip + "<br>Notes: " + labs[i].lab_pcs[j].notes + "<br>Activation Date : " + labs[i].lab_pcs[j].activationdate + "<br> Delivery Date: " + labs[i].lab_pcs[j].deliverydate;
					curr_pc_content.appendChild(curr_pc_content_text);
					curr_pc.addEventListener("click", function() {
						this.classList.toggle("active");
						var content = this.nextElementSibling;
						// If the content is hidden show it on click
						if (content.style.display === "block") {
						  content.style.display = "none";
						} else {
						  content.style.display = "block";
						}
					});
					curr_lab_content.appendChild(curr_pc);
					curr_lab_content.appendChild(curr_pc_content);

				}
				curr_lab.addEventListener("click", function() {
						this.classList.toggle("active");
						var content = this.nextElementSibling;
						// If the content is hidden show it on click
						if (content.style.display === "block") {
						  content.style.display = "none";
						} else {
						  content.style.display = "block";
						}
				});
				curr_complex_content.appendChild(curr_lab);
				curr_complex_content.appendChild(curr_lab_content);
			}
		}
		
		// Generate Labs list 
		// Append content box to collapsible and make it clickable
		// NOTE: the clickable part NEEDS to be here to due the async nature of AJAX
		main_div.appendChild(curr_complex_content);
		curr_complex.addEventListener("click", function() {
			this.classList.toggle("active");
			var content = this.nextElementSibling;
			// If the content is hidden show it on click
			if (content.style.display === "block") {
			  content.style.display = "none";
			} else {
			  content.style.display = "block";
			}
		});
	}
}
	
});
