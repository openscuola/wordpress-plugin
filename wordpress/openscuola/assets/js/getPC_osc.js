var app = {
	el: '#osc_pc',
	data() {
		return {
			pc_info: {
				donor: { name: "", code: "" },
				code: "",
				name: "",
				lshw: "",
				mac: "",
				mac_iface_out: "",
				model: "",
				notes: "",
				pn: "",
				sn: ""
			},
			position: {
				activationdate: "",
				deliverydate: "",
				hostname: "",
				ip: "",
				labcode: "",
				name: "",
				notes: "",
				offspring: "",
				user: ""
			},
			pc_id: getPC_osc_parameters.pc_id[0],
			pc_type: getPC_osc_parameters.pc_type[0],
			pc_picture: null,
			lshw_display: false,
			lshw_text: null,
			lshw_filename: null,
			info_obj_pc: null,
			info_obj_pos: null,
		}
	},
	mounted() {
		// Query PC info
		axios
			.get(getPC_osc_vars.base_dir + 'get_json_data.php', {
				params: {
					query: "getPC",
					pc_id: this.pc_id,
					pc_type: this.pc_type
				}
			})
			.then(response => {
				this.pc_info = response.data['pc_info']
				this.position = response.data['position']
				this.lshw_filename = this.pc_type + this.pc_id.padStart(3, '0');
				this.lshw_text = this.pc_info['lshw'];
				this.generateInfo();
			}
			)
		// Query PC images
		axios
			.get(getPC_osc_vars.base_dir + 'get_json_data.php', {
				params: {
					query: "getImages",
					gallery_id: "pc_pictures",
				}
			})
			.then(response => {
				pic_regex = new RegExp("^" + this.pc_type + "\\..*")
				for (var i = 0; i < response.data.length; i++) {
					// We want the file in the format PCTYPE.extension, otherwise we have a bunch of resolutions that we are not interested in
					if (pic_regex.test(response.data[i]) === true) {
						this.pc_picture = getPC_osc_vars.host_dir + "/wp-content/uploads/galleries/pc_pictures" + "/" + response.data[i];
					}
				}
			}
			)
	},
	methods: {
		saveTextAsFile: function (event) {
			textToWrite = this.lshw_text;
			filename = this.lshw_filename;

			var textFileAsBlob = new Blob([textToWrite], { type: 'text/plain' });

			var downloadLink = document.createElement("a");
			downloadLink.download = filename;
			downloadLink.innerHTML = "Download File";
			if (window.webkitURL != null) {
				// Chrome allows the link to be clicked
				// without actually adding it to the DOM.
				downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
			}
			else {
				// Firefox requires the link to be added to the DOM
				// before it can be clicked.
				downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
				downloadLink.onclick = function () {
					document.body.removeChild(downloadLink);
				};
				downloadLink.style.display = "none";
				document.body.appendChild(downloadLink);
			}
			downloadLink.click();
		},
		macExists: function () {
			return pc_info.mac_iface_out && pc_info.mac_iface_out != "n/a"
		},
		// Generate object with info
		generateInfo: function () {
			this.info_obj_pc = {
				"Modello": this.pc_info.model,
				"Part Number": this.pc_info.pn,
				"Serial Number": this.pc_info.sn,
				"Indirizzo MAC": this.pc_info.mac,
				"Donatore": this.pc_info.donor.name,
				"Note": this.pc_info.notes,
			};
			this.info_obj_pos = {
				"Laboratorio": this.position.name,
				"IP": this.position.ip,
				"Utente": this.position.user,
				"Hostname": this.position.hostname,
				"Data di consegna": this.position.deliverydate,
				"Data di attivazione": this.position.activationdate,
				"Frutto di rete": this.position.offspring,
				"Note": this.position.notes,
			}
		}
	}
}

Vue.createApp(app).mount('#osc_pc')
