var app = {
	el: '#osc_donor',
	data() {
		return {
			donor_id: getDonor_osc_parameters.donor_id[0],
			donor_info: {
				name:null,
				pc: [],

			},
			complex_list: new Array(),
			complex_map: new Map(),
			complex_display: new Array(),
			not_assigned: 0,
		}
	},
	mounted() {
		// Query complex info
		axios
			.get(getDonor_osc_vars.base_dir + 'get_json_data.php', {
				params: {
					query: "getDonor",
					donor_id: this.donor_id
				}
			})
			.then(response => {
				this.donor_info = response.data;
				// Create the complex lists that we will loop through using v-for
				this.assignPCtoComplex();
			}
			)
	},
	methods: {
		assignPCtoComplex: function () {
			// Create array of school objects to identify donated PCs
			for (var i = 0; i < this.donor_info.pc.length; i++) {
				curr_complex_id = this.donor_info.pc[i].complex.id;
				if (this.donor_info.pc[i].complex.id == undefined) {
					this.not_assigned++;
				}
				else if (this.complex_map.has(curr_complex_id)) {
					this.complex_list[this.complex_map.get(curr_complex_id)].pcs.push(this.donor_info.pc[i].pc);
				}
				else {
					complex_obj = {name: this.donor_info.pc[i].complex.name, 
						type: this.donor_info.pc[i].complex.type, 
						id: this.donor_info.pc[i].complex.id, 
						pcs: new Array()};
					complex_obj.pcs.push(this.donor_info.pc[i].pc);
					this.complex_list.push(complex_obj);
					this.complex_map.set(curr_complex_id, this.complex_list.length - 1);
				}
			}
			// Sort based on complex id
			this.complex_list = this.complex_list.sort(function (a, b) {
				if (a.id > b.id){
					return 1
				}
				else {
					return -1
				}
			});
			this.complex_display = new Array(this.complex_list.length);
			this.complex_display.fill(false);
		},
		toggleConfView: function(index){
			console.log(this.complex_display);
			this.complex_display[index] = !this.complex_display[index];
		},

	}
}

Vue.createApp(app).mount('#osc_donor')
