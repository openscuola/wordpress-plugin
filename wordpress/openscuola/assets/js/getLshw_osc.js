/* Get the div we are working on and assign it to a variable
we will only modify this element from now on and add whatever we want to it */

jQuery(document).ready(function(){

	var lshw=document.getElementById('osc_lshw');
	var lab_codes= new Array();
	jQuery.getJSON(getLshw_osc_vars.base_dir + 'get_json_data.php', {query: "getLshw", pc_id: getLshw_osc_parameters.pc_id[0], pc_type: getLshw_osc_parameters.pc_type[0]}, function (data) {
		// Preserve indentation and create box
		var pc_link=document.createElement("div");
		pc_link.setAttribute("class", "pc_link");
		pc_link.innerHTML= "<a href=../getPC/?pc_id=" + getLshw_osc_parameters.pc_id[0] + "&pc_type=" + getLshw_osc_parameters.pc_type[0] + "> Postazione: " + getLshw_osc_parameters.pc_type[0] + getLshw_osc_parameters.pc_id[0].padStart(3, '0') + "</a>";
		lshw.appendChild(pc_link);
		var lshw_pre=document.createElement("pre");
		var lshw_content=document.createTextNode(data.lshw);
		lshw_pre.appendChild(lshw_content);
		lshw.appendChild(lshw_pre);
		
	});
});