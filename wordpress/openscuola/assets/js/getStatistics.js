// This allows to create one class instance per statistics using wp_add_inline_script

class Statistic {
    // Initialize variables and do api call with axios
    constructor(el_id, statistic) {
      this.el_id = el_id;
      this.statistic = statistic;
      axios
      .get(getStatistics_osc_vars.base_dir + 'get_json_data.php', {
          params: {
              query: this.statistic
          }
      })
      .then(response => {
          console.log("Query done for: " + this.el_id )
          document.getElementById(this.el_id).innerHTML = response.data['response']
      }
      )
    }
  }


