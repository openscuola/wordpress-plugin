// Code for MAP

// Generate marker using the specified set of paramters, name status and type are all strings, labs and lab_status are array of strings, 
// the first representing the lab name and the second its status in readable format (NOT STATUS CODE), location is latitude and longitude
// in leaflet format

// Initialize variable for map, complexes and tiles
var map, complexes, basetile, firstLoad;

// Initialize variables for icons
var realizzatoIcon, programmatoIcon, sconosciutoIcon;
firstLoad = true;

// Initialize group of markers, in our case the "complexes" that will be queried from the DB
complexes = new L.MarkerClusterGroup({spiderfyOnMaxZoom: true, showCoverageOnHover: false, zoomToBoundsOnClick: true});

// Load the base tile, ie. the actual map

basetile = new L.TileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
	maxZoom: 18,
	subdomains: ["a", "b", "c"],
	attribution: 'Basemap tiles courtesy of <a href="http://www.openstreetmap.org/" target="_blank">OpenStreetMaps</a>. Map data (c) <a href="http://www.openstreetmap.org/" target="_blank">OpenStreetMap</a> contributors, CC-BY-SA.'
});

// Generate map by superimposing markers (complexes variable) and basetile 

map = new L.Map('map', {
	center: new L.LatLng(45.0735, 7.6756),
	maxZoom: 15,
	layers: [basetile, complexes]
});

// When document is ready query the database

realizzatoIcon = L.icon({
    iconUrl: getComplex_osc_vars.base_dir + 'assets/markers/marker-online.svg',

    iconSize:     [24, 32], // size of the icon
    shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [12, 32], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

programmatoIcon = L.icon({
    iconUrl:  getComplex_osc_vars.base_dir + 'assets/markers/marker-inprogress.svg',

    iconSize:     [24, 32], // size of the icon
    shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [12, 32], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

function genMarker(lat, lon, labs){
	var location = new L.LatLng(lat, lon);
	if (typeof labs !== 'undefined'){
		var iconTmp = realizzatoIcon;	
	}
	// If we have something inside the array, loop through the content and create a string that will be added to the Popup
	else {
		var iconTmp = programmatoIcon;
	}
	var marker = new L.Marker(location, {
		title: name,
		icon: iconTmp
	});
	complexes.addLayer(marker);
}

// Vue element with complex information

var app ={
	el: '#osc_complex',
	data() {
		return {
			complex_id: getComplex_osc_parameters.complex_id[0], 
			complex_info: {
				type: null,
				name: null,
				institute_text: {
					type: null,
					name:null,
				}
			},
			firstLoad: true,
		}
	},
	mounted() {
		// Query complex info, generate marker on map, fit map bounds
		axios
			.get(getComplex_osc_vars.base_dir + 'get_json_data.php', {
				params: {
					query: "getComplex",
					complex_id: this.complex_id
				}
			})
			.then(response => {
				this.complex_info = response.data;
				genMarker(this.complex_info.lat, this.complex_info.lon, this.complex_info.labs);
				if (this.firstLoad == true) {
					map.fitBounds(complexes.getBounds());
					this.firstLoad = false;
				}
			}
			)
	},
	methods: {

	}
}

Vue.createApp(app).mount('#osc_complex')
