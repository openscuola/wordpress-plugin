/* VUE */

var app = {
	el: '#listascuole',
	data() {
		return {
			complex_list: [],
			show_display: lista_osc_parameters.show,
			curr_filter_complex: "",
		}
	},
	mounted() {
		// Query complex info
		axios
			.get(lista_osc_vars.base_dir + 'get_json_data.php', {
				params: {
					query: "list",
				}
			})
			.then(response => {
				this.complex_list = response.data;
			}
			)
	},
	methods: {
		showComplex: function(curr_complex){
			// Check if in search 
			text = curr_complex.type_text + " " + curr_complex.name;
			text = text.toUpperCase();
			if (this.curr_filter_pc != "")
				if (!text.includes(this.curr_filter_complex.toUpperCase()))
					return false
			if (this.show_display == 'all'){
				return true;
			}
			else if (curr_complex.lab_status == undefined){
				if (lista_osc_parameters.show.includes('requested')){
					return true
				}
				else{
					return false;
				}
			}
			else {
				for (var i = 0; i<curr_complex.lab_status.length; i++){
					if (this.show_display.includes(curr_complex.lab_status[i])){
						return true
					}
	
				}
				return false
			}
		},
		computeAvailable: function(curr_complex){
			var available = 0;
			if (curr_complex.labs != undefined){
				for (var i = 0; i<curr_complex.labs.length; i++){
					if (this.show_display.includes(curr_complex.lab_status[i])){
						available++;
					}
				}
			}			
			return available;
		},
		computeRequested: function(curr_complex){
			var requested = 0;
			if (curr_complex.labs != undefined){
				for (var i = 0; i<curr_complex.labs.length; i++){
					if (this.show_display.includes(curr_complex.lab_status[i])){
						if ( curr_complex.lab_status[i] == "" )
						requested++;
					}
				}
			}
			return requested;
		}
	},
	computed: {
		activeComplexes: function(){
			if (this.complex_list.length > 0){
				return this.complex_list.filter(item => {
					return this.showComplex(item)
				})
		  	}
			return [];
		  
		}
	}
}

Vue.createApp(app).mount('#listascuole')


