// Create a clickable field by defining name and content
function createField(name, content, purehtml=0){
	if (purehtml == 1) {
		var createdField;
		createdField= "<b>" + name + ": </b>" + content;
	}
	else {
		var createdField=document.createElement("div");
		createdField.innerHTML= "<b>" + name + ": </b>" + content  + "<br>";
	}
	return createdField;
}

// Create a clickable field by defining name, content and URL, setting type to external will make the page open in another tab

function createClickableField(name, content, url, type, purehtml=0){
	if (type == "external"){
		var createdField=document.createElement("div");
		createdField.innerHTML= "<b>" + name + ": </b><a href=" + String(url) + " target='_blank'>" + String(content) + "<br>";
		if (purehtml == 1){
			return createdField.innerHTML;
		}
		else {
			return createdField;		
		}
	}
		
	var createdField=document.createElement("div");
	createdField.innerHTML= "<b>" + name + ": </b><a href=" + String(url) + ">" + String(content) + "<br>";
	if (purehtml == 1){
			return createdField.innerHTML;
		}
	else {
		return createdField;		
	}
}
