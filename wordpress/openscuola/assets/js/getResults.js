/* Get the div we are working on and assign it to a variable
we will only modify this element from now on and add whatever we want to it */

jQuery(document).ready(function ($) {
    jQuery.getJSON(getResults_osc_vars.base_dir + "assets/data/turin_suburbs.geojson", "", function (turin_suburbs_raw) {
        jQuery.getJSON(getResults_osc_vars.base_dir + 'get_json_data.php', { query: "map" }, function (laboratories) {
            turin_suburbs = turin_suburbs_raw.features.map(function (feature) {
                return turf.rewind(feature, { reverse: true });
            });

            lab_suburb = laboratories.map(function (laboratory) {
                for (var i = 0; i < turin_suburbs.length; i++) {
                    if (d3.geoContains(turin_suburbs[i], [laboratory['lon'], laboratory['lat']])) {
                        return turin_suburbs[i].id;
                    }
                }
                return -1;
            });

            color = d3.scaleQuantize([1, 7], d3.schemeBlues[6])
            turin_suburbs_withcnt = turin_suburbs.map(function (suburb_raw) {
                var suburb_new = suburb_raw;
                suburb_new['properties']['lab_count'] = lab_suburb.filter(element => element == suburb_new['id']).length
                return suburb_new
            });
            console.log("Hello");
            var main_div = d3.select("#map-results");

            const colorScale = d3.scaleOrdinal(d3.schemeAccent).domain(turin_suburbs_withcnt);

            var projection = d3.geoMercator();

            var path = d3.geoPath()
                .projection(projection);

            var tip = d3.tip()
                .attr('class', 'd3-tip')
                .offset([-5, 0])
                .html(function (d) {
                    var dataRow = d.properties.name;
                    if (dataRow) {
                        console.log(dataRow);
                        return d.properties.name + ": " + d.properties.lab_count;
                    } else {
                        console.log("no dataRow", d);
                        return "No data found";
                    }
                })




            var bounds = path.bounds({ "type": "FeatureCollection", "features": turin_suburbs_withcnt });
            var height_ratio = (bounds[1][1] - bounds[0][1]) / (bounds[1][0] - bounds[0][0]);

            const width = 100, height = Math.floor(width * height_ratio);
            console.log(height)
            console.log(width)
            projection.fitSize([width, height], { "type": "FeatureCollection", "features": turin_suburbs_withcnt })

            const svg = main_div.append("svg")
                .attr("viewBox", [0, 0, width, height])
                .attr("id", "svg-container");

            svg.call(tip);
            svg.append("g")
                .attr("transform", "translate(100,500)")
                .text("Laboratori Openscuola a Torino")

            svg.append("g")
                .selectAll("path")
                .data(turin_suburbs_withcnt)
                .attr("id", d => d.properties.name)
                .enter().append('path')
                .attr('d', path)
                .style('stroke', '#000')
                .style('stroke-width', '0.5px')
                .style('fill', d => color(d.properties.lab_count))
                .on('mouseover', tip.show)
                .on('mouseout', tip.hide)
                .on('click', function (d) {
                    center = d3.geoCentroid(d.geometry)
                    location.href = "risultati/?center_lat=" + center[1] + "&center_lon=" + center[0];
                })
                .classed("svg-content-responsive", true)
                .text(function (d) { return d.properties.lab_count; })

            //d3.select('g').style('transform', 'translate(0, 0)') 

            var text_div = document.querySelector("#text-results");
            var num_labs = 0;
            var num_zones = 0;
            console.log(laboratories)
            for (var i = 0; i < laboratories.length; i++) {
                if (laboratories[i].labs) {
                    for (var j = 0; j < laboratories[i].labs.length; j++) {
                        if (["R", "RA"].includes(laboratories[i].lab_status[j])) {
                            num_labs += 1;
                        }
                    }
                }
            }

            for (var i = 0; i < turin_suburbs_withcnt.length; i++) {
                if (turin_suburbs_withcnt[i].properties.lab_count) {
                    num_zones++;
                }
            }


            jQuery.getJSON(getResults_osc_vars.base_dir + 'get_json_data.php', { query: "getInstalled" }, function (resp) {
                $('#text-results').append("<div class='number-box'>" + num_labs + "</div>");
                $('#text-results').append("<div class='text-box'>I laboratori Openscuola realizzati</div>");
                $('#text-results').append("<div class='number-box'>" + laboratories.length + "</div>");
                $('#text-results').append("<div class='text-box'>I plessi che hanno fatto richiesta</div>");
                $('#text-results').append("<div class='number-box'>" + resp['response'] + "</div>");
                $('#text-results').append("<div class='text-box'>Le postazioni attualmente installate</div>");
                $('#text-results').append("<div class='number-box'>" + num_zones + "</div>");
                $('#text-results').append("<div class='text-box'>Le zone di Torino con i nostri laboratori</div>");
                number_boxes = document.getElementsByClassName("number-box");
                Array.from(number_boxes).forEach(element => {
                    animateValue(element, 0, element.innerHTML, 1000);
                });
            });            

        });
    });


})

function animateValue(obj, start, end, duration) {
    let startTimestamp = null;
    const step = (timestamp) => {
        if (!startTimestamp) startTimestamp = timestamp;
        const progress = Math.min(((timestamp - startTimestamp) / duration), 1);
        obj.innerHTML = Math.floor(progress * (end - start) + start);
        if (progress < 1) {
            window.requestAnimationFrame(step);
            console.log("Updating...")
        }
    };
    window.requestAnimationFrame(step);
}
