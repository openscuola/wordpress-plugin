// Initialize variable for map, complexes and tiles
var map, complexes, basetile, firstLoad;

// Initialize variables for icons
var realizzatoIcon, programmatoIcon, sconosciutoIcon;

// Initialize group of markers, in our case the "complexes" that will be queried from the DB
complexes = new L.MarkerClusterGroup({spiderfyOnMaxZoom: true, showCoverageOnHover: false, zoomToBoundsOnClick: true});

// Load the base tile, ie. the actual map

basetile = new L.TileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
	maxZoom: 18,
	subdomains: ["a", "b", "c"],
	attribution: 'Basemap tiles courtesy of <a href="http://www.openstreetmap.org/" target="_blank">OpenStreetMaps</a>. Map data (c) <a href="http://www.openstreetmap.org/" target="_blank">OpenStreetMap</a> contributors, CC-BY-SA.'
});

// Generate map by superimposing markers (complexes variable) and basetile 


if (mappa_osc_parameters.center != ''){
	console.log("Defined center: " + mappa_osc_parameters.center)
	var map_center = mappa_osc_parameters.center;
}
else{
	var map_center = [45.0735, 7.6756];
	// Resize to Labs only if center has not been set
	firstLoad = true;
}

map = new L.Map('map', {
	center: new L.LatLng(map_center[0], map_center[1]),
	zoom: 15,
	layers: [basetile, complexes]
});

// When document is ready query the database

jQuery(document).ready(function() {
	jQuery.ajaxSetup({cache:false});
	// Query the database (see getComplexes())
	getComplexes();
});

realizzatoIcon = L.icon({
    iconUrl: mappa_osc_vars.base_dir + 'assets/markers/marker-online.svg',

    iconSize:     [24, 32], // size of the icon
    shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [12, 32], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

programmatoIcon = L.icon({
    iconUrl: mappa_osc_vars.base_dir + 'assets/markers/marker-inprogress.svg',

    iconSize:     [24, 32], // size of the icon
    shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [12, 32], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

sconosciutoIcon = L.icon({
    iconUrl: mappa_osc_vars.base_dir + 'assets/markers/marker-unknown.svg',

    iconSize:     [28, 85], // size of the icon
    shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

// Setup legend box

var legend = L.control({position: 'bottomright'});

legend.onAdd = function (map) {
	// Create div with class legend (may be edited in mappa_osc.css)
	var realizzato_string="Relizzato";
	var programmato_string="Richiesto";
	
	// Generate different legend if we only have one parameter (or no legend at all since we only have one parameter?)
	
    var div = L.DomUtil.create("div", "legend");
	grades = ["Realizzato", "Richiesto"];
	// Initialize markers images
	labels = [mappa_osc_vars.base_dir+"assets/markers/marker-online.svg", mappa_osc_vars.base_dir+"assets/markers/marker-inprogress.svg"];
    // Simply fill up the HTML by looping through the previously created array, pretty straightforward to understand
	div.innerHTML += "<b>Stato laboratorio</b><br>"
    for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
            (" <img src="+ labels[i] +" height='20' width='20'>      ") + grades[i] + '<br>';
    }
	
    return div;
};

legend.addTo(map);

// Call the script "get_json_data.php" to get name, latitude and longitude in json format, then add the markers.

function getComplexes() {
	// Get JSON data
	
	jQuery.getJSON(mappa_osc_vars.base_dir + 'get_json_data.php', {query: "map"}, function (data) {
	// Query through array of data and add markers
	for (var i = 0; i < data.length; i++) {
		var location = new L.LatLng(data[i].lat, data[i].lon);
		console.log(mappa_osc_parameters.show);
		// Generate markers of the defined type (note that we are passing lab_status_text which is an actual string representing the status of the laboratory)
		// If we receive the default parameter we show all the laboratories
		if (mappa_osc_parameters.show == 'all'){
			genMarker(data[i].name, data[i].id, data[i].institute, data[i].status, data[i].type_text, data[i].address, data[i].tel, data[i].url, data[i].labs, data[i].lab_code,  data[i].lab_status_text, location);
		} 
		// If we receive an array then we show all the lab matching the parameters we received on the shortcode
		else{
			if (data[i].labs != null){
				var labs_tmp = new Array();
				var labs_text_tmp = new Array();
				var labs_codes_tmp = new Array();
				// Check whether the lab has to be inserted or not by looping through the array of parameters provided by the shortcode
				for (var j = 0; j<mappa_osc_parameters.show.length; j++){
					// Check whether we have a lab satisfying this condition
					for (var k=0; k<data[i].labs.length; k++){
						// Push to a temporary lab_text_tmp which will be passed to genMarker
						if (mappa_osc_parameters.show[j]==data[i].lab_status[k]){
							labs_tmp.push(data[i].labs[k]);
							labs_text_tmp.push(data[i].lab_status_text[k]);
							labs_codes_tmp.push(data[i].lab_code[k])
						}
					}
				}
				// Generate marker with correct set of labs
				if (labs_tmp.length > 0){
					genMarker(data[i].name, data[i].id, data[i].institute, data[i].status, data[i].type_text, data[i].address, data[i].tel, data[i].url, labs_tmp, labs_codes_tmp, labs_text_tmp, location);
				}
			}
			else if (mappa_osc_parameters.show.includes('requested')){
				genMarker(data[i].name, data[i].id, data[i].institute, data[i].status, data[i].type_text, data[i].address, data[i].tel, data[i].url, data[i].labs, data[i].lab_code,  data[i].lab_status_text, location);
			}	
		}
			
	}
		
	}).complete(function() {
	/*
	This piece of code allows the map to be dynamically resized when new markers are added, 
	however it needs to be enabled once the database has been completely filled.
	*/
	
	if (firstLoad == true) {
            map.fitBounds(complexes.getBounds());
            firstLoad = false;
          }
    });
}

// Generate marker using the specified set of paramters, name status and type are all strings, labs and lab_status are array of strings, 
// the first representing the lab name and the second its status in readable format (NOT STATUS CODE), location is latitude and longitude
// in leaflet format
function genMarker(name, id, institute, status, type, address, tel, url, labs, lab_code, lab_status, location){
	// If the labs array is undefined set the marker as "lab requested"
	if (typeof labs !== 'undefined'){
		var iconTmp = realizzatoIcon;
		var labs_list=new String("<br><b>Laboratori OpenScuola: </b><br>");
		for (var i = 0; i<labs.length; i++){
			labs_list=labs_list.concat("<b><a href=../getLab?lab_id=" + lab_code + ">" +labs[i] + "</b>: " + lab_status[i] + " <br> ");
		}
		
	}
	// If we have something inside the array, loop through the content and create a string that will be added to the Popup
	else {
		var labs_list=new String("<b>La scuola ha richiesto l'installazione dei laboratori </b><br>");
		var iconTmp = programmatoIcon;
	}
	// Create actual marker
	var marker = new L.Marker(location, {
		title: name,
		icon: iconTmp
	});
	// Setup popup content (ie. the div that appears when clicking on a complex)
	marker.bindPopup("<div style='text-align: left; margin-left: auto; margin-right: auto;'>"+ "<b> Plesso: </b><a href=../getComplex?complex_id=" + id + ">" + type + " " + name + "</a><br> <b>Istituto: </b><a href=../getInstitute?institute_id=" + institute.id + ">" + institute.type + " " + institute.name + "</a><br><b>Indirizzo: </b>"+ address + "<br> <b>Sito web:</b> <a href='" + url + "'>" + url + "</a><br><b> Telefono:</b> "+ tel + "<br>"+ labs_list + '</div>', {maxWidth: '400'});
	complexes.addLayer(marker);
}
	

// Enable clicking on markers
function onMapClick(e) {
	var markerLocation = new L.LatLng(e.latlng.lat, e.latlng.lng);
	var marker = new L.Marker(markerLocation);        
}
