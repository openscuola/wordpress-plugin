// This function creates the gallery if pictures are present

function galleryLoad(data, lab_id, gallery_div) {
	if (data != null) {
		console.log(this)
		// Configure gallery attributes
		gallery_div.setAttribute('class', 'gallery');
		gallery_div.setAttribute('itemscope', '');
		gallery_div.setAttribute('itemtype', 'http://schema.org/ImageGallery');

		images_div = document.createElement("div");
		images_div.setAttribute('class', 'images');

		thumbs_div = document.createElement("div");
		thumbs_div.setAttribute('class', 'thumbs');

		// setup regex to only get maximum resolution

		res_patt = /-\d{2,5}x\d{2,5}\..*/i;
		thumb_patt = /.*-1024*x[0-9]*.*/i;
		first = 1;

		// create laboratory images
		console.log(data)
		for (var i = 0; i < data.length; i++) {
			if (!res_patt.test(data[i])) {
				image_div = document.createElement("div");
				image_div.style.backgroundImage = "url(" + getLab_osc_vars.base_dir + "../../uploads/galleries/lab/" + lab_id + "/" + data[i] + ")";
				images_div.appendChild(image_div);
				if (first) {
					image_div.setAttribute('class', 'active');
					first = 0;
				}
			}
			else if (thumb_patt.test(data[i])) {
				thumb_div = document.createElement("div");
				thumb_div.style.backgroundImage = "url(" + getLab_osc_vars.base_dir + "../../uploads/galleries/lab/" + lab_id + "/" + data[i] + ")";
				thumbs_div.appendChild(thumb_div);
			}
		}

		// Add span elements for arrows
		span_left = document.createElement("span");
		span_left.setAttribute('class', 'left');
		span_right = document.createElement("span");
		span_right.setAttribute('class', 'right');

		images_div.appendChild(span_left);
		images_div.appendChild(span_right);

		gallery_div.appendChild(images_div);
		gallery_div.appendChild(thumbs_div);
		galleryInit(gallery_div);
	}
}

// This binds Vue to the osc_lab object

var app = {
	el: '#osc_lab',
	data() {
		return {
			lab_id: getLab_osc_parameters.lab_id[0],
			lab_info: {
				lab_pcs: [], lab_switches: [], name: null, code: null, complex: { name: null, type: null }
			},
			curr_filter_pc: "",
			curr_filter_switch: "",
			file_display: null,
			info_obj: null,
		}
	},
	mounted() {
		// Query LAB info
		axios
			.get(getLab_osc_vars.base_dir + 'get_json_data.php', {
				params: {
					query: "getLab",
					lab_id: this.lab_id
				}
			})
			.then(response => {
				this.lab_info = response.data;
				if (response.data.lab_files != undefined) {
					lab_files_length = response.data.lab_files.length;
				}
				else {
					lab_files_length = 0;
				}
				this.file_display = new Array(lab_files_length).fill(false)
				this.generateInfo();
			}
			)
		// Query gallery 
		axios
			.get(getLab_osc_vars.base_dir + 'get_json_data.php', {
				params: {
					query: "getLabImages",
					lab_id: getLab_osc_parameters.lab_id[0]
				}
			})
			.then(response => this.loadGallery(response.data))
	},
	methods: {
		// This method is called and allows saving of configuration files
		saveTextAsFile: function (index) {
			textToWrite = this.lab_info.lab_files[index].file;
			filename = this.lab_info.lab_files[index].name.split("/").pop();

			var textFileAsBlob = new Blob([textToWrite], { type: 'text/plain' });

			var downloadLink = document.createElement("a");
			downloadLink.download = filename;
			downloadLink.innerHTML = "Download File";
			if (window.webkitURL != null) {
				// Chrome allows the link to be clicked
				// without actually adding it to the DOM.
				downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
			}
			else {
				// Firefox requires the link to be added to the DOM
				// before it can be clicked.
				downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
				downloadLink.onclick = function () {
					document.body.removeChild(downloadLink);
				};
				downloadLink.style.display = "none";
				document.body.appendChild(downloadLink);
			}
			downloadLink.click();
		},
		// check whether we should be hiding a PC/switch according 
		// to the search term (by default everything is shown)
		filterPC: function (text) {
			if (this.curr_filter_pc != "")
				return text.includes(this.curr_filter_pc.toUpperCase())
			else
				return true
		},
		filterSwitch: function (text) {
			if (this.curr_filter_switch != "")
				return text.includes(this.curr_filter_switch.toUpperCase())
			else
				return true
		},
		toggleConfView: function (index) {
			console.log(this.file_display)
			this.file_display[index] = !this.file_display[index];
		},
		// Method wrapping the galleryLoad function (called once the REST API call 
		// has obtained gallery information)
		loadGallery: function (data) {
			galleryLoad(data, getLab_osc_parameters.lab_id[0], this.$refs.osc_gallery);
		},
		// Generate object with info
		generateInfo: function () {
			this.info_obj = {
				"Stato": this.lab_info.lab_status_text,
				"Plesso": this.lab_info.complex.name,
				"Consegna": this.lab_info.delivery,
				"Data adeguamento INVALSI": this.lab_info.INVALSI,
				"Aggiornamento OS": this.lab_info.OSupdate,
				"OS Primario": this.lab_info.OS,
				"OS Secondario": this.lab_info.SecondaryOS,
				"Banda interna del laboratorio": this.lab_info.Bandwidth,
				"Client": this.lab_info.client,
				"Note": this.lab_info.notes,
			}
		}
	},
	// This is why you should use computed properties and filter the users instead of blindly using v-if
	// https://medium.com/devmarketer/how-to-add-conditional-statements-to-v-for-loops-in-vue-js-c0b4d17e7dfd
	computed: {
		activePCs: function () {
			if (this.lab_info.lab_pcs.length > 0) {
				return this.lab_info.lab_pcs.filter(item => {
					return this.filterPC(item.pctype + item.pcid.padStart(3, '0'))
				})
			}
			return [];
		},
		activeSwitches: function () {
			if (this.lab_info.lab_switches != undefined)
			{
				if (this.lab_info.lab_switches.length > 0) {
					return this.lab_info.lab_switches.filter(item => {
						return this.filterSwitch(item.name)
					})
				}
			}
			
			return null;
		}
	}
}

Vue.createApp(app).mount('#osc_lab')
