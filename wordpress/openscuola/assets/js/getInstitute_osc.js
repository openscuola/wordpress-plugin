// Code for MAP

// Generate marker using the specified set of paramters, name status and type are all strings, labs and lab_status are array of strings, 
// the first representing the lab name and the second its status in readable format (NOT STATUS CODE), location is latitude and longitude
// in leaflet format

// Initialize variable for map, complexes and tiles


var map, complexes, basetile, firstLoad;

// Initialize variables for icons
var realizzatoIcon, programmatoIcon, sconosciutoIcon;
firstLoad = true;

// Initialize group of markers, in our case the "complexes" that will be queried from the DB
complexes = new L.MarkerClusterGroup({spiderfyOnMaxZoom: true, showCoverageOnHover: false, zoomToBoundsOnClick: true});

// Load the base tile, ie. the actual map

basetile = new L.TileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
	maxZoom: 18,
	subdomains: ["a", "b", "c"],
	attribution: 'Basemap tiles courtesy of <a href="http://www.openstreetmap.org/" target="_blank">OpenStreetMaps</a>. Map data (c) <a href="http://www.openstreetmap.org/" target="_blank">OpenStreetMap</a> contributors, CC-BY-SA.'
});

// Generate map by superimposing markers (complexes variable) and basetile 

map = new L.Map('map', {
	center: new L.LatLng(45.0735, 7.6756),
	maxZoom: 15,
	layers: [basetile, complexes]
});

// When document is ready query the database

realizzatoIcon = L.icon({
    iconUrl: getInstitute_osc_vars.base_dir + 'assets/markers/marker-online.svg',

    iconSize:     [28, 85], // size of the icon
    shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

function genMarker(lat, lon){
	var location = new L.LatLng(lat, lon);
	var marker = new L.Marker(location, {
		title: name,
		icon: realizzatoIcon
	});
	complexes.addLayer(marker);
}

// Enable clicking on markers
function onMapClick(e) {
	var markerLocation = new L.LatLng(e.latlng.lat, e.latlng.lng);
	var marker = new L.Marker(markerLocation);        
}

// Vue element with complex information

var app = {
	el: '#osc_institute',
	data() {
		return {
			institute_id: getInstitute_osc_parameters.institute_id[0],
			institute_info: {
				name: null,
				type: null
			},
			firstLoad: true,
			info_obj: null,
		}
	},
	mounted() {
		// Query complex info
		axios
			.get(getInstitute_osc_vars.base_dir + 'get_json_data.php', {
				params: {
					query: "getInstitute",
					institute_id: this.institute_id
				}
			})
			.then(response => {
				this.institute_info = response.data;
				genMarker(this.institute_info.lat, this.institute_info.lon, this.institute_info.labs);
				if (this.firstLoad == true) {
					map.fitBounds(complexes.getBounds());
					this.firstLoad = false;
				}
				this.generateInfo();
			}
			)
	},
	methods: {
		generateInfo: function(){
			this.info_obj = {
				"Indirizzo": this.institute_info.address,
				"Sito Web": this.institute_info.url,
				"E-Mail": this.institute_info.mail,
				"Telefono": this.institute_info.tel,
				"Note": this.institute_info.notes,
			};
		}
	}
}

Vue.createApp(app).mount('#osc_institute')
