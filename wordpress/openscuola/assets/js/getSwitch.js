// Vue element with complex information

var app = {
	el: '#osc_switch',
	data() {
		return {
			switch_name: getSwitch_osc_parameters.switch_name[0],
			switch_info: {name: null},
			info_obj: null,
		}
	},
	mounted() {
		// Query complex info
		axios
			.get(getSwitch_osc_vars.base_dir + 'get_json_data.php', {
				params: {
					query: "getSwitch",
					switch_name: this.switch_name
				}
			})
			.then(response => {
				this.switch_info = response.data;
				this.generateInfo();
			}
			)
	},
	methods: {
		generateInfo: function(){
			this.info_obj = {
				"Nome": this.switch_info.name,
				"Modello": this.switch_info.model,
				"IP": this.switch_info.ip,
				"Note": this.switch_info.notes,
				"Laboratorio": this.switch_info.lab.name,
			}
		}
	}
}

Vue.createApp(app).mount('#osc_switch')
